function dream_gui_popsteer(handles)
% dream_gui_popsteer(handles)
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Guangming Zhang, Fredrik Lingvall.

H=handles.pop_steer;
val = get(H,'Value');
     if val == 1
         pos='X';
     elseif val==2 
         pos='Y';
     else 
         pos='XY';
     end
     
switch pos
case 'X'
    set(handles.edit_steerx,'Visible','on');
    set(handles.text_steerx,'Visible','on');
    set(handles.text_steerxu,'Visible','on');
    set(handles.edit_steery,'Visible','off');
    set(handles.text_steery,'Visible','off');
    set(handles.text_steeryu,'Visible','off');
    
case 'Y'    
    set(handles.edit_steery,'Visible','on');
    set(handles.text_steery,'Visible','on');
    set(handles.text_steeryu,'Visible','on');
    set(handles.edit_steerx,'Visible','off');
    set(handles.text_steerx,'Visible','off');
    set(handles.text_steerxu,'Visible','off');

case 'XY'    
    set(handles.edit_steerx,'Visible','on');
    set(handles.edit_steery,'Visible','on');
    set(handles.text_steerx,'Visible','on');
    set(handles.text_steerxu,'Visible','on');
    set(handles.text_steery,'Visible','on');
    set(handles.text_steeryu,'Visible','on');
end

