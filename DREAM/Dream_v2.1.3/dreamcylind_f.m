function [H,err] = dreamcylind_f(Ro,geom_par,s_par,delay,m_par,err_level)
% [H,err] = dreamcylind_f(Ro,geom_par,s_par,delay,m_par,err_level)
%
% DREAMCYLIND_F - Computes the spatial impulse response
%  for a concave (focused) cylindrical transducer.
%
% Observation point(s) ([mm]):
% Ro = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN zoN]; 
%      (N is the number of observation points).
%
% Geometrical parameters:
% geom_par = [a b R];
%    a - x-size of the transducer.
%    b - y-size of the transducer.
%    R - Radius of the curvature.
%
% Sampling parameters:
% s_par = [dx dy dt nt];
%   dx [mm] - Spatial x-direction discretization size.
%   dy [mm] - Spatial y-direction discretization size.
%   dt [us] - Temporal discretization period (= 1/sampling freq).
%   nt      - Length of impulse response vector.
%
% Start point of SIR:
%   delay [us] - Scalar delay for all observation points or
%                a vector with individual delays for each observation point.
%
% Material parameters:
% m_par = [v cp alfa];
%   v    [m/s]      - Normal velocity.
%   cp   [m/s]      - Sound velocity.
%   alfa [dB/(cm MHz)] - Attenuation coefficient.
%
% Error Handling:
% err_level : Optional parameter. err_level is text string for
%             controlling the error behavior, options are:
%  'ignore' - An error is ignored (no error message is printed and
%              the program is not stopped) but the err output
%              argument is negative if an error occured.
%   'warn'   - An error message is printed but the program in not
%              stopped (and err is negative).\n\
%   'stop'   - An error message is printed and the program is stopped.
%
% dreamcylind_f is a MEX-function that is a part of the DREAM
% Toolbox available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2003,2008,2009 Fredrik Lingvall

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
