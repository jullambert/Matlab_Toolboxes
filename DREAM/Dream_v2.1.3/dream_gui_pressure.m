function pdata = dream_gui_pressure(data,param)
% pdata = dream_gui_pressure(data,
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.


ha=data;
dt=param{19};
delay=param{22};
[nt,nv]=size(ha);

%Compute pressure response
delta(1)=1/dt;delta(2)= -1/dt; for i=1:nv,hp(:,i)=convn(ha(:,i)',delta,'same')';end

%%%%%%%%%%Plot pressure response%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if size(ha,2)==1
     H=figure;
     set(H,'Name',['Untitled']);
     set(H,'NumberTitle','off');
     maxtime=nt*dt;
     x=delay:dt:(maxtime+delay);
     x=x(1:nt);
     plot(x,hp)
     axis tight
     title('Pressure Response');
     xlabel('Time [\mus]')
     ylabel('Pressure response hp(t)')
     grid
 else
     H=figure;
     set(H,'Name',['Untitled']);
     set(H,'NumberTitle','off');
     maxtime=nt*dt;
     y=delay:dt:(maxtime+delay);
     y=y(1:nt);
     if param{11}==2
         xmin=param{24}(1);
         xmax=param{24}(2);
     elseif param{11}==3
         xmin=param{25}(1);
         xmax=param{25}(2);
     elseif param{11}==4
         xmin=param{26}(1);
         xmax=param{26}(2);
     end    
     obse_n=param{27};
     step=(xmax-xmin)/(obse_n-1);
     x=xmin:step:xmax;
     x=x(1:obse_n);
     mesh(x,y,hp)
     axis tight
     title('Pressure Response');
     ylabel('Time [\mus]')
     if param{11}==2
         xlabel('Observation points(X) [mm]');
     elseif param{11}==3
         xlabel('Observation points(Y) [mm]')
     elseif param{11}==4 
         xlabel('Observation points(Z) [mm]')
     end
     zlabel('Pressure response hp(O,t)')
 end
