function Y = sum_fftconv_p(A,B,n_cpus,wisdom_str)
% Y = sum_fftconv_p(A,B,n_cpus,wisdom_str);
%     or sum_fftconv_p(A,B,n_cpus,Y,wisdom_str);
%
% SUM_FFTCONV_P - Computes (using parallel threaded processing) the
% sum of one dimensional convolutions of the columns in each matrix
% in the 3D matrix A with the corresponding columns in the matrix B.
%
% Normal mode
% ===========
%
% In normal mode sum_fftconv_p performs an operation similar to:
%
% YF = zeros(M+K-1,N);
% for l=1:L
%  for n=1:N
%    YF(:,n) = YF(:,n) + fft(A(:,n,l),M+K-1).* fft(B(:,l),M+K-1);
%   end
% end
% Y = real(ifft(Y));
%
% using n_cpus number of threads. The computations are performed
% using FFT:s from the fftw library.
%
%     Input parameters:
%
%    `A'
%          An MxNxL 3D matrix.
%
%    `B'
%          A KxL matrix.
%
%    `n_cpus'
%          Number of threads to use. n_cpus must be greater or equal to
%          1.
%
%    `wisdom_str'
%          Optional parameter. If the wisdom_str parameter is not
%          supplied then fftconv_p  calls fftw wisdom plan functions
%          before performing any frequency domain operations. This
%          overhead can be avoided by supplying a pre-computed fftw
%          wisdom string. For more information see the fftw user manunal
%          available at `http://www.fftw.org'.
%
% The wisdom_str can be obtained using the fftconv_p function. A
% typical example is,
%
%      % Compute a new fftw wisdom string.
%     [tmp,wisdom_str]  = fftconv_p(A(:,1,1),B(:,1),n_cpus);
%
%     for i=1:N
%
%       % Do some stuff here.
%
%       Y = sum_fftconv_p (A,B,n_cpus,wisdom_str);
%     end
%
% where the overhead of calling fftw plan functions is now avoided
% inside the for loop.
%
%     Output parameter:
%
%    `Y'
%          The (M+K-1)xN output matrix.
%
% In-place mode
% =============
%
%     In in-place mode sum_fftconv_p performs the operations in-place on
% a pre-allocated matrix. Here sum_fftconv_p do not have any output
% arguments and the results are instead stored directly in the
% pre-allocated (M+K-1)xN input matrix Y. A typical usage is:
%
%     Y = zeros(M+K-1,N);% Allocate space for Y.
%
%     for i=1:N
%
%       % Do some stuff here.
%
%        sum_fftconv_p(A,B,n_cpus,Y,wisdom_str);
%     end
%
% where memory allocation of the (possible large) matrix Y now is
% avoided inside the for loop.
%
%     NOTE: A side-effect of using in-place mode is that if a copy Y2 of
% Y is made then both Y2 and Y will be altered by sum_fftconv_p. That is,
% by performing,
% 
%     Y  = zeros(M+K-1,N);
%     Y2 = Y; % No actual copy of data here.
%     sum_fftconv_p(A,B,Y,n_cpus,wisdom_str);
%
% then both Y and Y2 will be changed (since Octave do not make a
% new copy of the data in Y2 unless Y2 is changed before the
% sum_fftconv_p call).
%
% sum_fftconv_p is a part of the DREAM Toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2006,2008,2009 Fredrik Lingvall.
%
% See also: fftconv_p, conv_p, fftconv, conv, fftw_wisdom.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
