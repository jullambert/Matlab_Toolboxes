function [H,err] = dream_arr_annu(Ro,G,s_par,delay,m_par,foc_met,focal,apod_met,apod,win_par,err_level)
% [H,err] = dream_arr_annu(Ro,G,s_par,delay,m_par,foc_met,focal,apod_met,apod,win_par,err_level)
%
% DREAM_ARR_ANNU - Computes the spatial impulse response
%  for an annular array.
%
% Observation point(s) ([mm]):
% Go = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN zoN]; 
%      (N is the number of observation points).
%
% Array grid parameter:  
% G [mm] - Vector of annulus radi.
%    
% Sampling parameters:
% s_par = [dx dy dt nt];
%   dx [mm] - Spatial x-direction discretization size.
%   dy [mm] - Spatial y-direction discretization size.
%   dt [us] - Temporal discretization size (= 1/sampling freq).
%   nt      - Length of impulse response vector.
%
% Start point of SIR:
%   delay [us] - Scalar delay for all observation points or
%                a vector with individual delays for each observation point.
%
% Material parameters:
% m_par = [v cp alfa];
%   v    [m/s]      - Normal velocity.
%   cp   [m/s]      - Sound velocity.
%   alfa [dB/(cm MHz)] - Attenuation coefficient.
%
% Focusing parameters:
% foc_met    - Focusing method, options are: 'on', 'off', or 'ud'
%              (user defined).
% focal [mm] - Focal distance. If foc_met = 'ud' (user defined) then focal is a
%              vector of focusing delays. 
%
% Apodization:
% apod_met - apodization method.
%   options:
%       'off'      - No apodization.
%       'ud'       - User defined apodization. 
%       'triangle' - Triangle window.
%       'gauss'    - Gaussian (bell-shaped) window.
%       'raised'   - Raised cosine
%       'simply'   - Simply supported
%       'clamped'  - Clamped.
% apod - Vector of apodiztion weights (used for the 'ud' option).
% win_par (scalar) - Parameter for raised cosine and Gaussian apodization functions.
%
% Error Handling:
% err_level : Optional parameter. err_level is text string for
%             controlling the error behavior, options are:
%   'ignore' - An error is ignored (no error message is printed and
%              the program is not stopped) but the err output
%              argument is negative if an error occured.
%   'warn'   - An error message is printed but the program in not
%              stopped (and err is negative).
%   'stop'   - An error message is printed and the program is stopped.
%
% dream_arr_annu is a MEX-function that is a part of the DREAM
% Toolbox available at `http://www.signal.uu.se/Toolbox/dream/'
%
% Copyright (C) 2003,2008,2009 Fredrik Lingvall.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
