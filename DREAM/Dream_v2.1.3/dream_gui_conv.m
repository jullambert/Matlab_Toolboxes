function dream_gui_conv(handles,data,param)
%function dream_gui_conv(handles,data,param)
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

ha=data;

% Creat a list selection dialog box.
ExciteForm{1} = 'Rayleigh ( t*exp(-param1*t^2)*sin(wt+0.75*pi); w=2*pi*freq. )';
ExciteForm{2} = 'Sine burst ( sin(w*t) with t<=param1; w=2*pi*freq. )';
ExciteForm{3} = 'Chebychev ( tanh(t/param1)*tanh(t/param)*(1-tanh(t/param2)*cos(wt); w=2*pi*freq. )';
ExciteForm{4} = 'Sine with exponential attenuation ( (exp(-param1*t))*sin(wt); w=2*pi*freq. )';

[s,v] = listdlg('PromptString','Select a excitation signal:',...
                      'SelectionMode','single',...
                      'ListString',ExciteForm);
if v==1 % OK is clicked.
    isig = s;
else
   return
end

% Create a dialog box to input param1, param2 and freq. Allow one line for each value.
if isig==3
   prompt  = {'Enter param1:','Enter param2:','Enter freq'};
   titlee   = 'Input the parameters of the selected excitation signal to convolve';
   lines= 1;
   def     = {'10','2','2'};
   ans  = inputdlg(prompt,titlee,lines,def);
   param1=str2num(ans{1}); param2=str2num(ans{2}); freq=str2num(ans{3});
else
   prompt = {'Enter param1:','Enter freq'};
   titlee = 'Input the parameters of the selected excitation signal to convolve';
   lines  = 1;
   def    = {'10','2'};
   ans    = inputdlg(prompt,titlee,lines,def);
   param1 = str2num(ans{1});
   freq   = str2num(ans{2});
   param2 = 10.0;
end
dt = param{19};
dens = param{17};
delay = param{22};
[nt,nv] = size(ha);

% Generate excitation signal
sig = excitation_signal(isig,freq,dt,param1,param2,nt);

% Compute pressure response if needed.
ipress=get(handles.cb_pressure,'Value');
if ipress==1,
  delta(1)=1/dt;
  delta(2)= -1/dt; 
  for i=1:nv,
    ha(:,i) = convn(ha(:,i)',delta,'same')';
  end
end
%Convolve
for i=1:nv,
  tmp=convn(ha(:,i)',sig)';
  hc(:,i)=tmp(1:nt);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%plot the selected excitation signal%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0
  H=figure;
  set(H,'Name',['Untitled']);
  set(H,'NumberTitle','off');
  row=size(sig,2);
  maxtime=row*dt;
  x=0:dt:(maxtime);
  x=x(1:row);
  plot(x,sig)
  axis tight
  title('Excitation Signal')
  xlabel('Time [\mus]')
  ylabel('Excitation form s(t)')
  grid
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%plot results of convolution%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if size(hc,2)==1
  H=figure;
  set(H,'Name',['Untitled']);
  set(H,'NumberTitle','off');
  maxtime=nt*dt;
  x=delay:dt:(maxtime+delay);
  x=x(1:nt);
  plot(x,hc)
  axis tight
  if ipress==0
    title('Convolution of Impulse Response')
    ylabel('Potential F(t)')
  else
       title('Convolution of Pressure Response')
       ylabel('Pressure P(t)')    
     end
     xlabel('Time [\mus]')
     grid
   else
     H=figure;
     set(H,'Name',['Untitled']);
     set(H,'NumberTitle','off');
     maxtime=nt*dt;
     y=delay:dt:(maxtime+delay);
     y=y(1:nt);
     if param{11}==2
         xmin=param{24}(1);
         xmax=param{24}(2);
     elseif param{11}==3
         xmin=param{25}(1);
         xmax=param{25}(2);
     elseif param{11}==4
         xmin=param{26}(1);
         xmax=param{26}(2);
     end    
     obse_n=param{27};
     step=(xmax-xmin)/(obse_n-1);
     x=xmin:step:xmax;
     x=x(1:obse_n);
     mesh(x,y,hc)
     axis tight
     if ipress==0
         title('Convolution of Impulse Response')
         zlabel('Potential F(t)')
     else
         title('Convolution of Pressure Response')
         zlabel('Pressure P(t)')    
     end
     ylabel('Time [\mus]')
     if param{11}==2
         xlabel('Observation points(X) [mm]');
     elseif param{11}==3
         xlabel('Observation points(Y) [mm]')
     elseif param{11}==4 
         xlabel('Observation points(Z) [mm]')
     end
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %%%plot amplitude%%%%%%%%%%%% 
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %ampl=max(hc);
     %H=figure;
     %set(H,'Name',['Untitled']);
     %set(H,'NumberTitle','off');
     %if param{11}==2
     %    xmin=param{24}(1);
     %    xmax=param{24}(2);
     %elseif param{11}==3
     %    xmin=param{25}(1);
     %    xmax=param{25}(2);
     %elseif param{11}==4
     %    xmin=param{26}(1);
     %    xmax=param{26}(2);
     %end    
     %obse_n=param{27};
     %step=(xmax-xmin)/(obse_n-1);
     %x=xmin:step:xmax;
     %x=x(1:obse_n);
     %plot(x,ampl)
     %axis tight
     %title('Maximum Amplitude')
     %if ipress==0
     %    ylabel('max(F(O))')
     %else
     %    ylabel('max(P(O))')
     %end
     %if param{11}==2
     %    xlabel('Observation points(X) [mm]');
     %elseif param{11}==3
     %    xlabel('Observation points(Y) [mm]')
     %elseif param{11}==4 
     %    xlabel('Observation points(Z) [mm]')
     %end
 end
 
 
%%%%%%%%%%%%%%Subfunction%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Generate excitation signal 
function sig = excitation_signal(isig,freq,dt,param1,param2,nt)
w=2*pi*freq;
sig=zeros(1,nt);
if isig==1,
    for i=1:nt,
       t = (i-1)*dt; 
       sig(i) = t*exp(-param1*t*t)*sin(w*t+0.75*pi);
    end
elseif isig==2,
    for i=1:inf
       t=(i-1)*dt;
       if t<=param1,
           sig(i) = sin(w*t);
       else
           sig = sig(1:i-1);
           break
       end
    end
elseif isig==3,
    for i=1:nt,
        t=(i-1)*dt;
        sig(i) = tanh(t/param1)^2*(1-tanh(t/param2))*cos(w*t);
    end
elseif isig==4,
    for i=1:nt,
        t = (i-1)*dt; 
	sig(i) = exp(-param1*t)*sin(w*t);
    end
end


   
     
    







