function dream_gui_close
% dream_gui_close
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

clear all
%called_win=gcbf;
%delete(called_win);
try
  called_win = wfindobj('figure','tag','dream');
  delete(called_win);
catch
  called_win=gcbf;
  delete(called_win);
end
