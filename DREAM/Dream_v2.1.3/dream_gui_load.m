function dream_gui_load(handles)
% dream_gui_load(handles)
%
% This is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

global Parameters data
%Display dialog boxto retrieve name of file for reading
[filename, pathname] = uigetfile( ...
       {
       '*.mat','MAT-files (*.mat)'; ...
       '*.*',  'All Files (*.*)'}, ...
        'Pick a file');
FILENAME=filename;
if isequal(filename,0)|isequal(pathname,0)
      disp('File did not find!');
      return
end
cd(pathname);
load(filename,'Parameters');
load(filename,'data');

%Restore old graphic interface and set parameters
SetAllParam(handles,Parameters);
dt=Parameters{19};
delay=Parameters{22};

if size(data,2)==1
     set(gcf,'Name',['DREAM - ',pathname,'\',filename]);
     H=figure;
     set(H,'Name',[pathname,'\',FILENAME]);
     set(H,'NumberTitle','off');
     row=size(data,1);
     maxtime=row*dt;
     x=delay:dt:(maxtime+delay);
     x=x(1:row);
     plot(x,data)
     axis tight
     title('Velocity Potential Impulse Response');
     xlabel('Time [\mus]')
     ylabel('Impulse response h(t)')
     grid
else
     set(gcf,'Name',['DREAM - ',pathname,'\',filename]);
     H=figure;
     set(H,'Name',[pathname,'\',FILENAME]);
     set(H,'NumberTitle','off');
     row=size(data,1);
     maxtime=row*dt;
     y=delay:dt:(maxtime+delay);
     y=y(1:row);
     if Parameters{11}==2
         xmin=Parameters{24}(1);
         xmax=Parameters{24}(2);
     elseif Parameters{11}==3
         xmin=Parameters{25}(1);
         xmax=Parameters{25}(2);
     elseif Parameters{11}==4
         xmin=Parameters{26}(1);
         xmax=Parameters{26}(2);
     end    
     obse_n=Parameters{27};
     step=(xmax-xmin)/(obse_n-1);
     x=xmin:step:xmax;
     x=x(1:obse_n);
     mesh(x,y,data)
     axis tight
     title('Velocity Potential Impulse Response');
     ylabel('Time [\mus]')
     if Parameters{11}==2
         xlabel('Observation points(X) [mm]');
     elseif Parameters{11}==3
         xlabel('Observation points(Y) [mm]')
     elseif Parameters{11}==4 
         xlabel('Observation points(Z) [mm]')
     end
     zlabel('Impulse response h(O,t)')
 end
