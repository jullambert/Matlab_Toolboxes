function dream_gui_help
% dream_gui_help
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

web(['file:///' which('dreamhelp.html')]);
