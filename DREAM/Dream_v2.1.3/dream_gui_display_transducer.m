function dream_gui_display_transducer(handles,ttype)
% dream_gui_display_transducer(handles,ttype)
%
% This a part of DREAM Toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

global imgfile
if get(handles.cb_himg,'Value')==0
   set(handles.pb_zoom,'Visible','on');
   switch ttype
   case 'Single'
        val = get(handles.pop_form_trans,'Value');
        if val == 1
            img='png/line.png';
        elseif val==2 
            img='png/rectang.png';
        elseif val==3
            img='png/rectang.png';
        elseif val==4
            img='png/circ.png';
        elseif val==5
            img='png/circ.png';
        elseif val==6
            img='png/circ_concave.png';        
        elseif val==7
            img='png/circ_convex.png';
        elseif val==8
            img='png/cyl_concave.png';
        elseif val==9
            img='png/cyl_convex.png';
        end
   case  'Array'   
        val = get(handles.pop_form_elem,'Value');
        if val == 1
           img='png/arr_rect_gui.png';
        elseif val==2   
           img='png/circ_arr_gui.png';
        elseif val==3   
           img='png/arr_rect.png'; % cylind_f
        elseif val==4   
           img='png/arr_rect.png'; % cylind_d
        elseif val==5   
           img='png/arr_annu.png';
        end
   end
   if(~strcmp(img,imgfile))
      imgfile=img;
      ext=imgfile(findstr(imgfile,'.')+1:length(imgfile));
      tmp=imgfile(1:findstr(imgfile,'.')-1);
      [A,idx]=imread(tmp,ext);
      axes(handles.axes_transducer);
      %colormap(gray);
      colormap(idx);
      %keyboard
      image(A);
      set(handles.axes_transducer,'Visible','off');
      axis off
   end
else
      set(handles.pb_zoom,'Visible','off');
end
