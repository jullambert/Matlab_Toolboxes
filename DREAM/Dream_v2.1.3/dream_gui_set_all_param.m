function dream_gui_set_all_param(handles,param)
% dream_gui_set_all_param(handles,param)
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

global ttype
%***********************************************************
%Restore all the parameters from a cell array 'Parameters'**
%***********************************************************
rb_single=param{1};
rb_array=param{2};
cb_grid1=param{3};
cb_grid2=param{4};
cb_focusing=param{5};
cb_steering=param{6};
cb_himg=param{7};
cb_pressure=param{8};
STran_type=param{9};
ETran_type=param{10};
obse_type=param{11};
focus_type=param{12};
steer_type=param{13};
weight_type=param{14};
Vel_of_Pro=param{15};
AbsCoef=param{16};
%%density=param{17}--not used;
Num_of_Sam=param{18};
deltat=param{19};
deltax=param{20};
deltay=param{21};
delay=param{22};
ExcVel=param{23};
obse_x=param{24};
obse_y=param{25};
obse_z=param{26};
obse_n=param{27};
Tsize_r=param{28};
Tsize_f=param{29};
Tsize_a=param{30};
Tsize_b=param{31};
G_Dx=param{32};
G_Dy=param{33};
G_Nx=param{34};
G_Ny=param{35};
focal=param{36};
steer_x=param{37};
steer_y=param{38};
weight_param=param{39};
cb_weight1=param{40};
cb_weight2=param{41};
G_filename=param{42};
W_filename=param{43};
focal_single = param{44};
single_focus_type = param{45};

%*****************************************************
%%%The following set the item of radio buttons and checkbox
%*****************************************************
set(handles.rb_single,'Value',rb_single);
set(handles.rb_array,'Value',rb_array);
if (rb_single==1)
    ttype='Single';
else 
    ttype='Array';
end
set(handles.cb_grid1,'Value',cb_grid1);
set(handles.cb_grid2,'Value',cb_grid2);
set(handles.cb_focusing,'Value',cb_focusing);
set(handles.cb_steering,'Value',cb_steering);
set(handles.cb_weight1,'Value',cb_weight1);
set(handles.cb_weight2,'Value',cb_weight2);
set(handles.cb_himg,'Value',cb_himg);
set(handles.cb_pressure,'Value',cb_pressure);

%*****************************************************
%%%The following set the item of popupmenu
%*****************************************************

% Form of single transducer.
set(handles.pop_form_trans,'Value',STran_type);

% Form of Element.
set(handles.pop_form_elem,'Value',ETran_type);

% Type of observation point.
set(handles.pop_observe,'Value',obse_type);

% Single transducer focusing.
set(handles.pop_single_focus,'Value',single_focus_type);

% Focusing of Arrays.
set(handles.pop_focus,'Value',focus_type);

% Beam steerig of Arrays.
set(handles.pop_steer,'Value',steer_type);

% Weight fucntion of arrays.
set(handles.pop_weight1,'Value',weight_type);

%****************************************************
%The following set the parameter values from 'edit'
%****************************************************

%%%%%%%%%%%Medium Parameters%%%%%%%%%%%%%

% Velocity of propagation
valstr=num2str(Vel_of_Pro);
set(handles.edit_p_v,'String',valstr);

% Absorption coefficient
valstr=num2str(AbsCoef);
set(handles.edit_absorpt,'String',valstr);

%Density of medium
%valstr=num2str(density);
%set(handles.edit_density,'String',valstr);

%%%%%%%%%%Parameters of computation%%%%%%%%%%

% Number of output samples
valstr=num2str(Num_of_Sam);
set(handles.edit_nt,'String',valstr);

% Averaging window width: Delta t
valstr=num2str(deltat);
set(handles.edit_deltat,'String',valstr);

% Surface discretization step: Delta x
valstr=num2str(deltax);
set(handles.edit_deltax,'String',valstr);

% Surface discretization step: Delta y
valstr=num2str(deltay);
set(handles.edit_deltay,'String',valstr);

% Delay time for display Impulse Response: Observe delay
valstr=num2str(delay);
set(handles.edit_delay,'String',valstr);

% Excitation velocity
valstr=num2str(ExcVel);
set(handles.edit_e_v,'String',valstr);

%%%%%%%%%Observation Point%%%%%%%%%%%%%%%%%%%%
%xo
valstr=mat2str(obse_x);
set(handles.edit_observex,'String',valstr);
%yo
valstr=mat2str(obse_y);
set(handles.edit_observey,'String',valstr);
%zo
valstr=mat2str(obse_z);
set(handles.edit_observez,'String',valstr);

%Number of points
valstr=num2str(obse_n);
set(handles.edit_observen,'String',valstr);

%%%%%%%%Size of Transducer%%%%%%%%%%%%%%%%%%%
%R
valstr=num2str(Tsize_r);
set(handles.edit_T_sizer,'String',valstr);

%F
valstr=num2str(Tsize_f);
set(handles.edit_T_sizef,'String',valstr);
%a
valstr=num2str(Tsize_a);
set(handles.edit_T_sizea,'String',valstr);
%b
valstr=num2str(Tsize_b);
set(handles.edit_T_sizeb,'String',valstr);

%%%%%%%%%%%%%%Definition of Element Grid%%%%%%%
%filename
set(handles.edit_G_filename,'String',G_filename);
%Dx
valstr=num2str(G_Dx);
set(handles.edit_G_Dx,'String',valstr);
%Dy
valstr=num2str(G_Dy);
set(handles.edit_G_Dy,'String',valstr);
%Nx
valstr=num2str(G_Nx);
set(handles.edit_G_Nx,'String',valstr);
%Ny
valstr=num2str(G_Ny);
set(handles.edit_G_Ny,'String',valstr);

%%%%%%%%% Single transducer focusing %%%%%%%%%%%%%%%%%%
valstr=num2str(focal_single);
set(handles.edit_single focal,'String',valstr);

%%%%%%%%% Focusing of Arrays %%%%%%%%%%%%%%%%%%
valstr=num2str(focal);
set(handles.edit_focal,'String',valstr);

%%%%%%%%% Beam steerig of Arrays %%%%%%%%%%%%%%%%%%
valstr=num2str(steer_x);
set(handles.edit_steerx,'String',valstr)
valstr=num2str(steer_y);
set(handles.edit_steery,'String',valstr);

%%%%%%%%%%%%Weight Function%%%%%%%%%%%%%%%%%%%%%%
%filename
set(handles.edit_W_filename,'String',W_filename);
%Parameter of weighting function
valstr=num2str(weight_param);
set(handles.edit_weight1,'String',valstr);


%***************************************************
%********Restore the graphic interface**************
%***************************************************
dream_gui_layout(handles,ttype)
