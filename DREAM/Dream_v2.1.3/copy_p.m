function copy_p(B,row_idx,col_idx,A,n_cpus)
% copy_p(B,row_idx,col_idx,A,n_cpus)
%
% COPY_P Parallel (threaded) in-place matrix copy.
%
% Input parameters:
%
% B                 - Pre-allocated Output matrix of size >= (r2-r1)x(c2-c1).
% row_idx = [r1 r2] - Two element vector defining rows in B where
%                     the data is copied.
% col_idx = [c1 c2] - Two element vector defining columns in B where
%                     the data is copied.
% B                 - Input matrix of size (r2-r1)x(c2-c1);
% n_cpus            - Number of threads to use. n_cpus must be greater or equal to 1.
%
% copy_p is a MEX-function that is a part of the DREAM Toolbox at
% `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2003,2008,2009 Fredrik Lingvall

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
