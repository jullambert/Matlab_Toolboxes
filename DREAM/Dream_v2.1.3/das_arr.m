function [H,err] = das_arr(Ro,G,s_par,delay,m_par,foc_met,...
    focal,steer_met,steer_par,apod_met,apod,win_par,err_level)
% [H,err] = das_arr(Ro,G,s_par,delay,m_par,foc_met,...
%     focal,steer_met,steer_par,apod_met,apod,win_par,err_level)
%
% DAS_ARR Computes the delay reponse for array transducers. That is,
% DAS_ARR only computes the delay to each observation point which is
% represented by a '1' at the corresponding data point.
%
% Observation point(s) ([mm]):
% Ro = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN zoN]; 
%      (N is the number of observation points).
%
% Array grid parameter: 
%   G  - Lx3 grid function matrix. Column 1 contain the x-postions 
%      of the elements, column 2 the y-positions, and column 3
%      the z-positions (L is the number of elements).
%
% Sampling parameters:
% s_par = [dt nt];
%   dt [us] - Temporal discretization period (= 1/sampling freq).
%   nt      - Length of impulse response vector.
%
% Start point of SIR:
%   delay [us] - Scalar delay for all observation points or
%                a vector with individual delays for each observation point.
%
% Material parameters:
% m_par = [cp];
%   cp   [m/s] - Sound velocity.
%
% Focusing parameters:
% foc_met    - Focusing method, options are: 'off', 'x', 'y', 'xy',
%              and 'x+y'.
% focal [mm] - Focal distance.
%
% Beam steering:
% steer_met - Beam steering method, options are: 'off', 'x', 'y',
%             and 'xy'.
% steer_par =  [theta phi];
%   theta [deg] - x-direction steer angle.
%   phi   [deg] - y-direction steer angle.
%
% Apodization:
% apod_met - apodization method.
%   options:
%       'off'      - No apodization.
%       'ud'       - Used defined apodization. 
%       'triangle' - Triangle window.
%       'gauss'    - Gaussian (bell-shaped) window.
%       'raised'   - Raised cosine
%       'simply'   - Simply supported
%       'clamped'  - Clamped.
% apod - Vector of apodiztion weights (used for the 'ud' option).
% win_par (scalar) - Parameter for raised cosine and Gaussian apodization functions.
%
% Error Handling:
% err_level : Optional parameter. err_level is text string for
%             controlling the error behavior, options are:
%   'ignore' - An error is ignored (no error message is printed and
%              the program is not stopped) but the err output
%              argument is negative if an error occured.
%   'warn'   - An error message is printed but the program in not
%              stopped (and err is negative).
%   'stop'   - An error message is printed and the program is stopped.
%
% das_arr is a MEX-function that is a part of the DREAM Toolbox
% available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2008,2009 Fredrik Lingvall

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
