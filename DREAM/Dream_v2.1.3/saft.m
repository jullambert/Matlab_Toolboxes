function y = saft(B,To,delay,s_par,m_par,Ro,a)
% y = saft(B,To,delay,s_par,m_par,Ro,a)
%
% Time-domain synthetic aperture focusing techninque (SAFT) 
% with linear interpolation.
%
%  --- Delay-and-sum algorithm ---
%
% B-scan image:
% B   - KxL Ultrasonic B-scan data matrix.
%
% Transducer positions ([mm]):
% To = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoL yoL zoL]; 
%      where L is the number of transducer positions.
%
% Delay (starting point(s) of the A-scans):
% delay [us]
%
% Sampling parameters: s_par = [dt];
%   dt [us] - Temporal discretization period (= 1/sampling freq).
%
% Material parameters:
% m_par = [cp];
%   cp   [m/s]      - Sound velocity.
%
% Observation point(s) ([mm]):
% Ro = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN zoN]; 
%      where N is the number of observation points.
%
% Synthetic aperture:
% a [mm]
%
% Output data:
% y  - Processed data (use reshape to form an image).
%
% saft is a MEX-function that is a part of the DREAM Toolbox
% available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2008,2009 Fredrik Lingvall.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
