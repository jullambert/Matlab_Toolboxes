function varargout = dream_gui(varargin)
% DREAM_GUI is a graphical user interface front-end to the DREAM
% toolbox that is used calculate the impulse reponses of single 
% ultrasonic transducers and transducer arrays.
%
% Details about its theory refers the following publication
% B.Piwakowski and K.Sbai, IEEE on UFFFC, vol.46,No.2,1999.
%
% DREAM_gui Application M-file for DREAM_GUI.fig
%
%  fig = dream_gui launch DREAM GUI.
%  DREAM('callback_name', ...) invoke the named callback.
%
% This file is a part of the DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.



if nargin == 0  % LAUNCH GUI
  
  fig = openfig(mfilename,'reuse');
  
  % Generate a structure of handles to pass to callbacks, and store it. 
  handles = guihandles(fig);
  guidata(fig, handles);
  
  % Initialize some Parameters and the graphical user interface.
  global imgfile ttype 
  imgfile='circular1.jpg';
  ttype='Single';
  dream_gui_layout(handles,ttype);
  if nargout > 0
    varargout{1} = fig;
  end
  
elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
  %disp(varargin{1})
  try
    [varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
  catch
    disp(lasterr);
  end
  
end

%
% Callbacks.
%

% --------------------------------------------------------------------
function varargout = pb_calculate_Callback(h, eventdata, handles, varargin)
global data Parameters
global ttype
[data,Parameters] = dream_gui_calc(handles,ttype);

% --------------------------------------------------------------------
function varargout = pb_saveas_Callback(h, eventdata, handles, varargin)
global data Parameters
dream_gui_save(data,Parameters);

% --------------------------------------------------------------------
function varargout = pb_load_Callback(h, eventdata, handles, varargin)
dream_gui_load(handles)
% --------------------------------------------------------------------
function varargout = pb_close_Callback(h, eventdata, handles, varargin)
dream_gui_close;
% --------------------------------------------------------------------
function varargout = pb_spectrum_Callback(h, eventdata, handles, varargin)
global data Parameters
dream_gui_spectrum(handles,data,Parameters);

% --------------------------------------------------------------------
function varargout = pb_pressure_Callback(h, eventdata, handles, varargin)
global data Parameters
dream_gui_pressure(data,Parameters)

% --------------------------------------------------------------------
function varargout = pb_convolution_Callback(h, eventdata, handles, varargin)
global data Parameters
dream_gui_conv(handles,data,Parameters)

% --------------------------------------------------------------------
function varargout = pb_zoom_Callback(h, eventdata, handles, varargin)
global ttype
dream_gui_zoom;

% --------------------------------------------------------------------
function varargout = edit_density_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_e_v_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_absorpt_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_deltat_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_deltax_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_p_v_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_delay_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_deltay_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_nt_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_T_sizer_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_deltar_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_T_sizea_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_T_sizeb_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_focal_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_steery_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_steerx_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_observen_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_observey_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_observex_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_observez_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = pop_weight1_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = pop_form_trans_Callback(h, eventdata, handles, varargin)
dream_gui_layout(handles,'Single');
% --------------------------------------------------------------------
function varargout = pop_observe_Callback(h, eventdata, handles, varargin)
dream_gui_opoint_type(handles);

% --------------------------------------------------------------------
function varargout = pop_steer_Callback(h, eventdata, handles, varargin)
dream_gui_popsteer(handles);

% --------------------------------------------------------------------
function varargout = pop_focus_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = rb_single_Callback(h, eventdata, handles, varargin)
global ttype
if get(handles.rb_single,'Value')==1
     set(handles.rb_array,'Value',0);
     ttype='Single';
else
    set(handles.rb_array,'Value',1);
    ttype='Array';
end     
dream_gui_layout(handles,ttype);

% --------------------------------------------------------------------
function varargout = rb_array_Callback(h, eventdata, handles, varargin)
global ttype
if get(handles.rb_array,'Value')==1
  set(handles.rb_single,'Value',0);
  ttype='Array';
else
  set(handles.rb_single,'Value',1);
  ttype='Single';
end     
dream_gui_layout(handles,ttype);

% --------------------------------------------------------------------
function varargout = pop_form_elem_Callback(h, eventdata, handles, varargin)
dream_gui_layout(handles,'Array')


% --------------------------------------------------------------------
function varargout = cb_grid1_Callback(h, eventdata, handles, varargin)
if get(handles.cb_grid1,'Value')==1
     set(handles.cb_grid2,'Value',0);
else
    set(handles.cb_grid2,'Value',1);
end   
dream_gui_layout(handles,'Array')
% --------------------------------------------------------------------
function varargout = cb_grid2_Callback(h, eventdata, handles, varargin)
if get(handles.cb_grid2,'Value')==1
     set(handles.cb_grid1,'Value',0);
else
    set(handles.cb_grid1,'Value',1);
end 
dream_gui_layout(handles,'Array')
% --------------------------------------------------------------------
function varargout = edit_G_Nx_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_G_Ny_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_G_Dx_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = edit_G_Dy_Callback(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = cb_focusing_Callback(h, eventdata, handles, varargin)
%Parameters of Array Focusing.
if(get(handles.cb_focusing,'Value')==1)
  
  % Only on/off focusing for Annular array (= item 5 in menu).
  if get(handles.pop_form_elem,'Value') ~=5
    set(handles.pop_focus,'Enable','on');
  end

  set(handles.edit_focal,'Enable','on');
else
  set(handles.pop_focus,'Enable','off');
  set(handles.edit_focal,'Enable','off');
end

% --------------------------------------------------------------------
% Beamsteering.
function varargout = cb_steering_Callback(h, eventdata, handles, varargin)
if(get(handles.cb_steering,'Value')==1)      
   set(handles.pop_steer,'Enable','on');
   set(handles.edit_steerx,'Enable','on');
   set(handles.edit_steery,'Enable','on');
   dream_gui_popsteer(handles);
else    
   set(handles.pop_steer,'Enable','off');
   set(handles.edit_steerx,'Enable','off');
   set(handles.edit_steery,'Enable','off');
end

% --------------------------------------------------------------------
function varargout = pb_dreamhelp_Callback(h, eventdata, handles, varargin)
dream_gui_help

% --------------------------------------------------------------------
function varargout = cb_weight2_Callback(h, eventdata, handles, varargin)
if get(handles.cb_weight2,'Value')==1
     set(handles.cb_weight1,'Value',0);
     set(handles.edit_weight1,'Enable','off');
     set(handles.pop_weight1,'Enable','off');
     set(handles.edit_W_filename,'Enable','on');
else
     set(handles.cb_weight1,'Value',1);
     set(handles.edit_weight1,'Enable','on');
     set(handles.pop_weight1,'Enable','on');
     set(handles.edit_W_filename,'Enable','off');
end   
% --------------------------------------------------------------------
function varargout = cb_weight1_Callback(h, eventdata, handles, varargin)
if get(handles.cb_weight1,'Value')==1
    set(handles.cb_weight2,'Value',0); 
    set(handles.edit_weight1,'Enable','on');
    set(handles.pop_weight1,'Enable','on');
    set(handles.edit_W_filename,'Enable','off');
else
     set(handles.cb_weight2,'Value',1);
     set(handles.edit_weight1,'Enable','off');
     set(handles.pop_weight1,'Enable','off');
     set(handles.edit_W_filename,'Enable','on');
end   

% --------------------------------------------------------------------
function varargout = cb_pressure_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = cb_himg_Callback(h, eventdata, handles, varargin)
global imgfile ttype
if get(handles.cb_himg,'Value')==1
     set(handles.pb_zoom,'Visible','off');
     axes(handles.axes_transducer)
     cla
     reset(gca)
     set(handles.axes_transducer,'Visible','off');
else
     %if(isempty(imgfile))
     dream_gui_display_transducer(handles,ttype)
     %end
     set(handles.pb_zoom,'Visible','on');
     ext=imgfile(findstr(imgfile,'.')+1:length(imgfile));
     tmp=imgfile(1:findstr(imgfile,'.')-1);
     [A,idx] = imread(tmp,ext);
     axes(handles.axes_transducer);
     %colormap(gray);
     colormap(idx);
     image(A);
     set(handles.axes_transducer,'Visible','off');
     axis off
end     

% --------------------------------------------------------------------
function varargout = edit_weight1_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_W_filename_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = edit_G_filename_Callback(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = pb_about_ButtonDownFcn(h, eventdata, handles, varargin)

fid_ver = fopen('VERSION','r'); 	% Open version file.
fid_date = fopen('DATE','r'); 		% Open date file.

string{1} = 'The DREAM Toolbox';

% Version.
string{2} = char(fread(fid_ver,'char'))';
string{2} = ['Version ' string{2}];
L = length(string{2});
string{2} = string{2}(1:L-1); % Remove CR.

% Date
string{3} = char(fread(fid_date,'char'))';
L = length(string{3});
string{3} = string{3}(1:L-1); % Remove CR.
%string{3} = 'October 9, 2003';
msgbox(string,'About DREAM');

% --------------------------------------------------------------------
function varargout = dream_ResizeFcn(h, eventdata, handles, varargin)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over edit_G_Ny.
function edit_G_Ny_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to edit_G_Ny (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


