function [Y] = scirc_sir(Ro,geom_par,delay,s_par,m_par,n_int)
%[Y] = scirc_sir(Ro,geom_par,delay,s_par,m_par,n_int)
%
% SCIRC_SIR Computes the sampled time-continous (analytic) spatial impulse
%  response(s) for a circular transducer.
%
% Observation point(s) ([mm]):
%
% `Ro'
%    An N x 3 matrix, Ro = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN
%    zoN]; where N is the number of observation points.
%
% Geometrical parameters: geom_par = [r];
%
% `r'
%    Radius of the transducer [mm].
%
% Sampling parameters: s_par = [dt nt];
%
% `dt'
%    Temporal discretization period (= 1/sampling freq) [us].
%
% `nt'
%   Length of impulse response vector.
%
% Start point of SIR:
%
% `delay'
%   Scalar delay for all observation points or a vector with
%   individual delays for each observation point [us].
%
% Material parameters: m_par = [v cp];
%
% `v'
%   Normal velocity [m/s].
%
% `cp'
%   Sound velocity [m/s].
%
% Number of temporal inegration points:
%
% `n_int'
%   Scalar the number of points used for for sampling the
%   analytic SIR in each sampling interval.
%
% scirc_sir is an MEX-function that is a part of the DREAM Toolbox
% available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2008,2009 Fredrik Lingvall.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
