function param = dream_gui_get_all_param(handles)
% param = dream_gui_get_all_param(handles)
%
% param is a cell array
%
% This file is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

%%*****************************************************
%%%The following get the item of radio button and checkbox
%*****************************************************
rb_single = get(handles.rb_single,'Value');
rb_array = get(handles.rb_array,'Value');
cb_grid1 = get(handles.cb_grid1,'Value');
cb_grid2 = get(handles.cb_grid2,'Value');
cb_focusing = get(handles.cb_focusing,'Value');
cb_steering = get(handles.cb_steering,'Value');
cb_weight1 = get(handles.cb_weight1,'Value');
cb_weight2 = get(handles.cb_weight2,'Value');
cb_himg = get(handles.cb_himg,'Value');
cb_pressure = get(handles.cb_pressure,'Value');

%*****************************************************
%%%The following get the item of popupmenus
%*****************************************************

% Form of single transducer.
STran_type = get(handles.pop_form_trans,'Value');

% Form of Element.
ETran_type = get(handles.pop_form_elem,'Value');

% Type of observation point.
obse_type = get(handles.pop_observe,'Value');

% Single transducer focusing.
single_focus_type= get(handles.pop_single_focus,'Value');

% Focusing of Arrays.
focus_type= get(handles.pop_focus,'Value');

% Beam steerig of Arrays.
steer_type= get(handles.pop_steer,'Value');

% Weight fucntion of arrays.
weight_type= get(handles.pop_weight1,'Value');

%****************************************************
%The following get the parameter values from 'edit'
%****************************************************
%%%%%%%%%%%Medium Parameters%%%%%%%%%%%%%
% Velocity of propagation
valstr=get(handles.edit_p_v,'String');
Vel_of_Pro=str2num(valstr);
% Absorption coefficient
valstr=get(handles.edit_absorpt,'String');
AbsCoef=str2num(valstr);
%Density of medium
%valstr=get(handles.edit_density,'String');
%density=str2num(valstr);

%%%%%%%%%%Parameters of computation%%%%%%%%%%
%Number of output samples
valstr=get(handles.edit_nt,'String');
Num_of_Sam=str2num(valstr);
%Averaging window width: Delta t
valstr=get(handles.edit_deltat,'String');
deltat=str2num(valstr);
%Surface discretization step: Delta x
valstr=get(handles.edit_deltax,'String');
deltax=str2num(valstr);
%Surface discretization step: Delta y
valstr=get(handles.edit_deltay,'String');
deltay=str2num(valstr);
%Delay time for display Impulse Response: Observe delay
valstr=get(handles.edit_delay,'String');
delay=str2num(valstr);
%Excitation velocity
valstr=get(handles.edit_e_v,'String');
ExcVel=str2num(valstr);

%%%%%%%%% Observation Point %%%%%%%%%%%%%%%%%%%%
%X0
valstr=get(handles.edit_observex,'String');
obse_x=str2num(valstr);
%Y0
valstr=get(handles.edit_observey,'String');
obse_y=str2num(valstr);
%Z0
valstr=get(handles.edit_observez,'String');
obse_z=str2num(valstr);
%Number of points
valstr=get(handles.edit_observen,'String');
obse_n=str2num(valstr);

%%%%%%%% Size of Transducer %%%%%%%%%%%%%%%%%%%
%r
valstr=get(handles.edit_T_sizer,'String');
Tsize_r=str2num(valstr);
%R
valstr=get(handles.edit_T_sizef,'String');
Tsize_R=str2num(valstr);
%a
valstr=get(handles.edit_T_sizea,'String');
Tsize_a=str2num(valstr);
%b
valstr=get(handles.edit_T_sizeb,'String');
Tsize_b=str2num(valstr);

%%%%%%%%% Single Transducer Focusing %%%%%%%%%%%%%%%%%%
valstr= get(handles.edit_single_focal,'String');
focal_single=str2num(valstr);

%%%%%%%%%%%%%% Definition of Element Grid %%%%%%%
%filename
G_filename=get(handles.edit_G_filename,'String');
%dx
valstr=get(handles.edit_G_Dx,'String');
G_Dx=str2num(valstr);
%dy
valstr=get(handles.edit_G_Dy,'String');
G_Dy=str2num(valstr);
%Nx
valstr=get(handles.edit_G_Nx,'String');
G_Nx=str2num(valstr);
%Ny
valstr=get(handles.edit_G_Ny,'String');
G_Ny=str2num(valstr);

%%%%%%%%% Array Focusing %%%%%%%%%%%%%%%%%%
valstr= get(handles.edit_focal,'String');
focal=str2num(valstr);

%%%%%%%%% Array Beam steerig %%%%%%%%%%%%%%%%%%
valstr= get(handles.edit_steerx,'String');
steer_x=str2num(valstr);
valstr= get(handles.edit_steery,'String');
steer_y=str2num(valstr);

%%%%%%%%% Array Weight function %%%%%%%%%%%%%%%%
%filename
W_filename=get(handles.edit_W_filename,'String');
%parameter of weighting function
valstr= get(handles.edit_weight1,'String');
weight_param=str2num(valstr);

%**************************************************
%Save all the parameters in a cell array 'param'
%**************************************************
param{1}=rb_single;
param{2}=rb_array;
param{3}=cb_grid1;
param{4}=cb_grid2;
param{5}=cb_focusing;
param{6}=cb_steering;
param{7}=cb_himg;
param{8}=cb_pressure;
param{9}=STran_type;
param{10}=ETran_type;
param{11}=obse_type;
param{12}=focus_type;
param{13}=steer_type;
param{14}=weight_type;
param{15}=Vel_of_Pro;
param{16}=AbsCoef;
param{17}=-1;%-not used %%density;
param{18}=Num_of_Sam;
param{19}=deltat;
param{20}=deltax;
param{21}=deltay;
param{22}=delay;
param{23}=ExcVel;
param{24}=obse_x;
param{25}=obse_y;
param{26}=obse_z;
param{27}=obse_n;
param{28}=Tsize_r;
param{29}=Tsize_R;
param{30}=Tsize_a;
param{31}=Tsize_b;
param{32}=G_Dx;
param{33}=G_Dy;
param{34}=G_Nx;
param{35}=G_Ny;
param{36}=focal;
param{37}=steer_x;
param{38}=steer_y;
param{39}=weight_param;
param{40}=cb_weight1;
param{41}=cb_weight2;
param{42}=G_filename;
param{43}=W_filename;
param{44}=focal_single;
param{45}=single_focus_type;
