function dream_gui_zoom
% dream_gui_zoom
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

global imgfile

ext=imgfile(findstr(imgfile,'.')+1:length(imgfile));
tmp=imgfile(1:findstr(imgfile,'.')-1);
[A,idx]=imread(tmp,ext);
H=figure;
%colormap(gray);
colormap(idx);
image(A);
axis off
