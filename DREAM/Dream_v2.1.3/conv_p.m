function [Y] = conv_p(A,B,n_cpus)
% [Y] = conv_p(A, B, n_cpus).
%
% CONV_P Computes one dimensional convolutions of the columns in
% the matrix A and the matrix (or vector) B.
%
% Input parameters:
%
%  A      - An MxN matrix.\n\
%  B      - A KxN matrix or a K-length vector. If B is a vector each 
%           column in A is convolved with the vector B.
%  n_cpus - Number of threads to use. n_cpus must be greater or equal to 1.
%
% Output parameter:
%
% Y - The (M+K-1)xN output matrix.
%
% conv_p is a mex-function that is a part of the DREAM Toolbox 
% available at http://www.signal.uu.se/Toolbox/dream/.
%
% See also fftconv_p, conv.
%
% Copyright (C) 2003,2008,2009 Fredrik Lingvall
%
% See also: fftconv_p, fftconv, conv.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $x



