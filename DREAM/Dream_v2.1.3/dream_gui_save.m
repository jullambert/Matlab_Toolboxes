function dream_gui_save(data,Parameters)
% dream_gui_save(data,Parameters)
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.


if isempty(data)
   errordlg('Have you made a calculation? Please try again!','Error Message');    
   return
end
[filename, pathname] = uiputfile( ...
       {
        '*.mat','MAT-files (*.mat)'; ...
        '*.*',  'All Files (*.*)'}, ...
        'Save as');
if filename==0
     return
end   
cd(pathname);

set(gcf,'Name',['Discrete Represnetation Array Modelling (DREAM) - ',pathname,'\',filename]);
try 
   save(filename, 'Parameters','data');
catch 
   errordlg('Data have not been saved. Please try again!','Error Message');    
end
