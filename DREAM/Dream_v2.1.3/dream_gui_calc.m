function [data,Parameters] = dream_gui_calc(handles,ttype)
% [data,Parameters] = dream_gui_calc(handles,ttype)
% 
% This file is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

% Error constants.
NONE   = 0;
IGNORE = -1;
WARN   = -2;
STOP   = -3;


data       = []; % Avoid warning message.
Parameters = dream_gui_get_all_param(handles);
param      = Parameters;

%************************************************************************
%******************  Call MEX functions      ****************************
%************************************************************************

% Common parameters.
cp    = param{15};  			% Soundspeed.
alfa  = param{16}; 			% Attenuation cofficeint
nt    = param{18}; 			% Length of SIRs.
dt    = param{19}; 			% Temporal sampling period.

dx    = param{20}; 
dy    = param{21}; 
delay = param{22};			% Observation delay.
v     = param{23};			% Normal surface velocity.

% Observation point.
xo = param{24}; 
yo = param{25}; 
zo = param{26};
tmp = zo.*1000/cp;

%if(delay>tmp)
%  delay=tmp;  param{22}=tmp;
%  valstr=num2str(param{22});
%  set(handles.edit_delay,'String',valstr);
%  warndlg('Observation Delay is out of range! The default value is used.','Warning Message'); 
%end



% Tranducer Type
switch ttype
  
  case 'Single'

    %
    % Single transducer focusing.
    %
    ifoc = param{45}+1;
    if ifoc == 1 % Not used.
      foc_met='off'; 
    elseif ifoc == 2
      foc_met='x';
    elseif ifoc == 3; 
      foc_met='y';
    elseif ifoc == 4
      foc_met='xy';
    elseif ifoc == 5
      foc_met='x+y';
    end
    
    val = param{9};
    if val == 1
      TransType='Line';
    elseif val==2 
      TransType='rectangular';
    elseif val==3
      TransType='Frectangular';
    elseif val==4
      TransType='circular';
    elseif val==5
      TransType='Fcircular';         
    elseif val==6
      TransType='cir_concave';
    elseif val==7
      TransType='cir_convex';
    elseif val==8
      TransType='cyl_concave';
    elseif val==9
      TransType='cyl_convex';
    end
    
  case  'Array'   

    %
    % Array focusing method: 'off'(ifoc=1),'x'(ifoc=2),'y'(ifoc=3),'xy'(ifoc=4),'x+y'(ifoc=5)
    %
    ifoc = param{5}+1;
    if(ifoc==1)
      focal = 0;
    else
      ifoc = param{12}+1; 
      focal =param{36};
    end
    
    if ifoc == 1 
      foc_met='off';
    elseif ifoc == 2
      foc_met='x';
    elseif ifoc == 3; 
      foc_met='y';
    elseif ifoc == 4
      foc_met='xy';
    elseif ifoc == 5
      foc_met='x+y';
    end

    val = param{10};
    if val == 1
      TransType='E_rect';
    elseif val==2 
      TransType='E_cir';
    elseif val==3
      TransType='E_cyl_concave';
    elseif val==4
      TransType='E_cyl_convex';
    elseif val==5
      TransType='E_Annular';
    end
    
    % Preparration of parameters for Array transducer
    % Steering parameter; ister=1,2,3,4 corresponds steering type respectively:'off','x','y','xy'.
    ister = param{6};
    if ister==0
      steer_met='off';
      %steer_par=[teta,fi];%teta: steering angle in x-direction;fi: steering angle in y-direction.
      steer_par=[0,0];
    else
      ister = param{13};
      if ister==1
	steer_met='x';
      elseif ister==2
	steer_met='y';
      elseif ister==3
	steer_met='xy';
      end
      steer_par=[param{37},param{38}];
    end

    iweight=param{40};
    % Select a existing weighting function
    if(iweight==1)
      iapo = param{14};
      if iapo==1
	apod_met='off';
      elseif iapo==2
	apod_met='triangle';
      elseif iapo==3
	apod_met='gauss';
      elseif iapo==4
	apod_met='raised';
      elseif iapo==5
	apod_met='simply';
      elseif iapo==6
	apod_met='calmped';
      end
      apod = [];
    else
      apod_met='ud';%User defines weighting function
      %W_filename=param{43};
      if isempty(param{43})
	errordlg('No weighting file!','Error Message'); 
      else
	fid = fopen(param{43},'r');
	if fid < 0
	  errordlg('Apodization file not found!','Error Message'); 
	end
	fclose(fid);
	apod = textread(param{43},'%f');
      end
    end

    %cb_grid1=param{3};
    if(param{3}==1)
      %G_filename=param{42};
      if isempty(param{42})
	errordlg('No grid file!','Error Message'); 
      else
	fid = fopen(param{42},'r');
	if fid < 0
	  errordlg('Grid file not found!','Error Message'); 
	end
	fclose(fid);
	[gx,gy,gz] = textread(param{42},'%f%f%f');
      end
    end
    
    win_par=param{39}; % Parametr for raised cosine and Gaussian apodization functions.

end

switch TransType
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  % Single element transducers
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %
  % Line transducer.
  %
  case 'Line'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamline...');
    drawnow;
    %xsmin=param{28}; xsmax=param{29};
    a=param{28};
    %if isempty(xsmin)|isempty(xsmax)
    if isempty(a)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    
    if param{11}==2 % if observation points is line along X-axis
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo = xomin+step*(i-1);
	delay = zo.*1000/cp;
	[data(:,i),err] = ...
	    dreamline([xo,yo,zo],a,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % if observation points is line along Y-axis.
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err]=dreamline([xo,yo,zo],a,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4 % if observation points is line along Z-axis.
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err]=dreamline([xo,yo,zo],a,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err]=dreamline([xo,yo,zo],a,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end 
    
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Rectangular transducer.
    %
  case 'rectangular'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamrect...');
    drawnow;
    %xsmin=param{28}; xsmax=param{29};ysmin=param{30};ysmax=param{31};
    %if isempty(xsmin)|isempty(xsmax)|isempty(ysmin)|isempty(ysmax)
    a=param{28}; b=param{29};
    if isempty(a)|isempty(b)
    errordlg('Transducer size parameter is missing!','Error Message'); 
    return
  end
  
  if param{11}==2 % Observation points is line along X-axis.
    xomin=param{24}(1);
    xomax=param{24}(2);
    obse_n=param{27};
    step=(xomax-xomin)/(obse_n-1);
    for i=1:obse_n
      xo=xomin+step*(i-1);
      delay=zo.*1000/cp;
      [data(:,i),err] = dreamrect([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
  elseif param{11}==3 % Observation points is line along Y-axis.
    yomin=param{25}(1);
    yomax=param{25}(2);
    obse_n=param{27};
    step=(yomax-yomin)/(obse_n-1);
    for i=1:obse_n
      yo=yomin+step*(i-1);
      delay=zo.*1000/cp;
      [data(:,i),err] = dreamrect([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end 
  elseif  param{11}==4 % Observation points is line along Z-axis.
    zomin=param{26}(1);
    zomax=param{26}(2);
    obse_n=param{27};
    step=(zomax-zomin)/(obse_n-1);
    for i=1:obse_n
      zo=zomin+step*(i-1);
      delay=zo.*1000/cp;
      [data(:,i),err] = dreamrect([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end 
  else % A single observation point.
    [data,err] = dreamrect([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
    if err ~= NONE
      errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	  ,'Error Message');
    end
  end
  set(handles.text_mexfunc,'String','Done!');
  
    %
    % Focused rectangular transducer.
    %
  case 'Frectangular'
    
    set(handles.text_mexfunc,'String','Calling mexfunction dreamrect_f...');
    drawnow;
    %xsmin=param{28}; xsmax=param{29};ysmin=param{30};ysmax=param{31};
    %if isempty(xsmin)|isempty(xsmax)|isempty(ysmin)|isempty(ysmax)
    a=param{28}; b=param{29}; focal=param{44};
    if isempty(a)|isempty(b)|isempty(focal)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    if param{11}==2 % if observation points is line along X-axis
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = ...
	    dreamrect_f([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamrect_f([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamrect_f([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else % A  single observation point.
      [data,err] = dreamrect_f([xo,yo,zo],[a,b],[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
  case 'circular'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamcirc...');
    drawnow;
    r=param{28};
    if isempty(r)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    %%%%%if observation points is line along X-axis
    if param{11}==2
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcirc([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3%%if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcirc([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcirc([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err]=dreamcirc([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Focused circular transducer.
    %
  case 'Fcircular'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamcirc_f...');
    drawnow;
    r=param{28};focal=param{44};
    if isempty(r)|isempty(focal)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end

    if param{11}==2 % Observation points is line along X-axis.
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcirc_f([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3%%if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err]=dreamcirc_f([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err]=dreamcirc_f([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err]=dreamcirc_f([xo,yo,zo],r,[dx,dy,dt,nt],delay,[v,cp,alfa],foc_met,focal,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Circular concave transducer.
    %
  case 'cir_concave'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamspher_f...');
    r=param{28}; R=param{29};
    if isempty(r)|isempty(R)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    if R<r
      errordlg('Focal length (R) smaller than transducer radius (r). Please input parameters again!','Error Message'); 
      return
    end
    %%%%%if observation points is line along X-axis
    if param{11}==2
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamspher_f([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3%%if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamspher_f([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamspher_f([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err] = dreamspher_f([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Circular convex transducer.
    %
  case 'cir_convex'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamspher_d...');
    drawnow;
    r=param{28}; R=param{29};
    if isempty(r)|isempty(R)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    if R<r
      errordlg('Focal length (R) smaller than transducer radius (r). Please input parameters again!','Error Message'); 
      return
    end
    %%%%%if observation points is line along X-axis
    if param{11}==2
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamspher_d([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3%%if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamspher_d([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamspher_d([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err] = dreamspher_d([xo,yo,zo],[r,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Cylindrical focused transducer.
    % 
  case 'cyl_concave'
    drawnow;
    set(handles.text_mexfunc,'String','Calling mexfunction dreamcylind_f...');
    a=param{28}; b=param{29};R=param{30};
    if isempty(a)|isempty(b)|isempty(R)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    %%%%%if observation points is line along X-axis
    if param{11}==2
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcylind_f([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3%%if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcylind_f([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcylind_f([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err] = dreamcylind_f([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Cylindrical convex transducer.
    %
  case 'cyl_convex'
    set(handles.text_mexfunc,'String','Calling mexfunction dreamcylind_d...');
    drawnow;
    a=param{28}; b=param{29};R=param{30};
    if isempty(a)|isempty(b)|isempty(R)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    %%%%%if observation points is line along X-axis
    if param{11}==2
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcylind_d([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');         
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3%%if observation points is line along Y-axis
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcylind_d([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');         
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4%%if observation points is line along Z-axis
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp;
	[data(:,i),err] = dreamcylind_d([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');         
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else%%For the single observation point
      [data,err] = dreamcylind_d([xo,yo,zo],[a,b,R],[dx,dy,dt,nt],delay,[v,cp,alfa],'ignore');         
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Array transducers
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %
    % Array with rectangular elements.
    %
  case 'E_rect'
    set(handles.text_mexfunc,'String','Calling mexfunction dream_arr_rect...');
    drawnow;
    a=param{28}; b=param{29};
    cb_grid2=param{4};
    if(cb_grid2==1)
      G_Dx=param{32}; G_Dy=param{33};
      G_Nx=param{34};G_Ny=param{35};
      gxr=(-floor(G_Nx/2):1:ceil(G_Nx/2)-1)*G_Dx;
      gyc=(-floor(G_Ny/2):1:ceil(G_Ny/2)-1)*G_Dy;
      gx=[];gy=[];
      for i=1:G_Ny
	gx = [gx,gxr];
	gyr = ones(1,G_Nx)*gyc(i);
	gy = [gy,gyr];
      end
      gz = zeros(1,G_Nx*G_Ny);
    end
    gx=gx(:); gy=gy(:); gz=gz(:);

    if param{11}==2 % Observation points is line along X-axis.
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err] = dream_arr_rect([xo,yo,zo],[a,b],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % Observation points is line along Y-axis.
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_rect([xo,yo,zo],[a,b],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4 % Observation points is line along Z-axis.
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_rect([xo,yo,zo],[a,b],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else % A single observation point.
      [data,err]=dream_arr_rect([xo,yo,zo],[a,b],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	  foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Array with circular elements.
    %
  case 'E_cir'
    set(handles.text_mexfunc,'String','Calling mexfunction dream_arr_circ...');
    drawnow;
    r=param{28}; cb_grid2=param{4};
    if(cb_grid2==1)
      G_Dx=param{32}; G_Dy=param{33};
      G_Nx=param{34};G_Ny=param{35};
      gxr=(-floor(G_Nx/2):1:ceil(G_Nx/2)-1)*G_Dx;
      gyc=(-floor(G_Ny/2):1:ceil(G_Ny/2)-1)*G_Dy;
      gx=[];gy=[];
      for i=1:G_Ny
	gx=[gx,gxr];
	gyr=ones(1,G_Nx)*gyc(i);
	gy=[gy,gyr];
      end
      gz=zeros(1,G_Nx*G_Ny);
    end
    gx=gx(:);gy=gy(:);gz=gz(:);

    if param{11}==2 % Observation points is line along X-axis.
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_circ([xo,yo,zo],r,[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % Observation points is line along Y-axis.
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_circ([xo,yo,zo],r,[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4 % Observation points is line along Z-axis.
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_circ([xo,yo,zo],r,[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else % A single observation point.
      [data,err]=dream_arr_circ([xo,yo,zo],r,[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	  foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Array with focused cylindrical elements.
    %
  case 'E_cyl_concave';
    set(handles.text_mexfunc,'String','Calling mexfunction dream_arr_cylind_f...');
    drawnow;
    a=param{28}; b=param{29};R=param{30};
    if isempty(a)|isempty(b)|isempty(R)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    cb_grid2=param{4};
    if(cb_grid2==1)
      G_Dx=param{32}; G_Dy=param{33};
      G_Nx=param{34};G_Ny=param{35};
      gxr=(-floor(G_Nx/2):1:ceil(G_Nx/2)-1)*G_Dx;
      gyc=(-floor(G_Ny/2):1:ceil(G_Ny/2)-1)*G_Dy;
      gx=[];gy=[];
      for i=1:G_Ny
	gx=[gx,gxr];
	gyr=ones(1,G_Nx)*gyc(i);
	gy=[gy,gyr];
      end
      gz=zeros(1,G_Nx*G_Ny);
    end
    gx=gx(:);gy=gy(:);gz=gz(:);

    if param{11}==2 % Observation points is line along X-axis.
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_cylind_f([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % Observation points is line along Y-axis.
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_cylind_f([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4 % Observation points is line along Z-axis.
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_cylind_f([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      end 
    else % A single observation point.
      [data,err]=dream_arr_cylind_f([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	  foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Array with cylindrical convex elements.
    %
  case 'E_cyl_convex';    
    
    set(handles.text_mexfunc,'String','Calling mexfunction dream_arr_cylind_d...');
    drawnow;
    a=param{28}; b=param{29};R=param{30};
    if isempty(a)|isempty(b)|isempty(R)
      errordlg('Transducer size parameter is missing!','Error Message'); 
      return
    end
    cb_grid2=param{4};
    if(cb_grid2==1)
      G_Dx=param{32}; G_Dy=param{33};
      G_Nx=param{34};G_Ny=param{35};
      gxr=(-floor(G_Nx/2):1:ceil(G_Nx/2)-1)*G_Dx;
      gyc=(-floor(G_Ny/2):1:ceil(G_Ny/2)-1)*G_Dy;
      gx=[];gy=[];
      for i=1:G_Ny
	gx=[gx,gxr];
	gyr=ones(1,G_Nx)*gyc(i);
	gy=[gy,gyr];
      end
      gz=zeros(1,G_Nx*G_Ny);
    end
    gx=gx(:);gy=gy(:);gz=gz(:);

    if param{11}==2 % Observation points is line along X-axis.
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_cylind_d([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % Observation points is line along Y-axis.
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_cylind_d([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4 % Observation points is line along Z-axis.
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err]=dream_arr_cylind_d([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else % A single observation point.
      [data,err]=dream_arr_cylind_d([xo,yo,zo],[a,b,R],[gx,gy,gz],[dx,dy,dt,nt],delay,[v,cp,alfa],...
	  foc_met,focal,steer_met,steer_par,apod_met,apod,win_par,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
    
    %
    % Annular array.
    %
  case 'E_Annular'
    set(handles.text_mexfunc,'String','Calling mexfunction dream_arr_annu...');
    drawnow;
    R0 = param{28}; W = param{29}; d = param{30};
    cb_grid2=param{4};
    % Annular array only has two options
    if ~strcmp(foc_met,'off')
      foc_met = 'on';
    end
    if(cb_grid2==1)
      N=param{34}; gr=[];
      if N<=1
	gr=R0;
      else
	for i=1:N-1
	  gr=[gr,R0+i*d+(i-1)*W,R0+i*d+i*W];
	end
	gr=[R0,gr];  
      end
    end
    gr=gr(:);
    if param{11}==2 % Observation points is line along X-axis.
      xomin=param{24}(1);
      xomax=param{24}(2);
      obse_n=param{27};
      step=(xomax-xomin)/(obse_n-1);
      for i=1:obse_n
	xo=xomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err] = dream_arr_annu([xo,yo,zo],gr,[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end
    elseif param{11}==3 % Observation points is line along Y-axis.
      yomin=param{25}(1);
      yomax=param{25}(2);
      obse_n=param{27};
      step=(yomax-yomin)/(obse_n-1);
      for i=1:obse_n
	yo=yomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err] = dream_arr_annu([xo,yo,zo],gr,[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    elseif  param{11}==4 % Observation points is line along Z-axis.
      zomin=param{26}(1);
      zomax=param{26}(2);
      obse_n=param{27};
      step=(zomax-zomin)/(obse_n-1);
      for i=1:obse_n
	zo=zomin+step*(i-1);
	delay=zo.*1000/cp-1;
	[data(:,i),err] = dream_arr_annu([xo,yo,zo],gr,[dx,dy,dt,nt],delay,[v,cp,alfa],...
	    foc_met,focal,apod_met,apod,win_par,'ignore');
      	if err ~= NONE
	  errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	      ,'Error Message');
	end
      end 
    else % A single observation point.
      [data,err] = dream_arr_annu([xo yo zo],gr,[dx dy dt ...
	    nt],delay,[v cp alfa],foc_met,focal,apod_met,apod,win_par,'ignore');
      if err ~= NONE
	errordlg('Impulse response out-of-bounds. Adjust length and/or delay' ...
	    ,'Error Message');
      end
    end
    set(handles.text_mexfunc,'String','Done!');
end

%%%%%%%%%% Plot impulse responses %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(data,2)==1
  H=figure;
  set(H,'Name',['Untitled']);
  set(H,'NumberTitle','off');
  row=size(data,1);
  maxtime=row*dt;
  x=delay:dt:(maxtime+delay);
  x=x(1:row);
  plot(x,data)
  axis tight
  title('Spatial (Velocity Potential) Impulse Response');
  xlabel('Time [\mus]')
  ylabel('Spatial Impulse response h(t)')
  grid
else
  H=figure;
  set(H,'Name',['Untitled']);
  set(H,'NumberTitle','off');
  row=size(data,1);
  maxtime=row*dt;
  y=delay:dt:(maxtime+delay);
  y=y(1:row);
  if param{11}==2
    xmin=param{24}(1);
    xmax=param{24}(2);
  elseif param{11}==3
    xmin=param{25}(1);
    xmax=param{25}(2);
  elseif param{11}==4
    xmin=param{26}(1);
    xmax=param{26}(2);
  end    
  obse_n=param{27};
  step=(xmax-xmin)/(obse_n-1);
  x=xmin:step:xmax;
  x=x(1:obse_n);
  mesh(x,y,data)
  axis tight
  title('Spatial (Velocity Potential) Impulse Response');
  ylabel('Time [\mus]')
  if param{11}==2
    xlabel('Observation points (X) [mm]');
  elseif param{11}==3
    xlabel('Observation points (Y) [mm]')
  elseif param{11}==4 
    xlabel('Observation points (Z) [mm]')
  end
  zlabel('Spatial Impulse response h(O,t)')
end
