function dream_gui_layout(handles,ttype)
% dream_gui_layout(handles,ttype)
%
% This function is a part of the DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

switch ttype
  case 'Single'
    % Type of Transducer"
    set(handles.pop_form_trans,'Enable','on');
    %set(handles.text_form_trans,'Enable','on');
    set(handles.pop_form_elem,'Enable','off');
    %set(handles.text_form_elem,'Enable','off');
    % Grid parameters (Arrays).
    set(handles.cb_grid1,'Enable','off');
    set(handles.cb_grid2,'Enable','off');
    set(handles.edit_G_Dx,'Enable','off');
    set(handles.edit_G_Dy,'Enable','off');
    set(handles.edit_G_Nx,'Enable','off');
    set(handles.edit_G_Ny,'Enable','off');
    set(handles.edit_G_filename,'Enable','off');
    % Weighting Function (Arrays).
    set(handles.cb_weight1,'Enable','off');
    set(handles.cb_weight2,'Enable','off');
    set(handles.edit_weight1,'Enable','off');
    set(handles.pop_weight1,'Enable','off');
    set(handles.edit_W_filename,'Enable','off');
    % Parameters of Beam Steering (Arrays).
    set(handles.cb_steering,'Enable','off');
    set(handles.pop_steer,'Enable','off');
    set(handles.edit_steerx,'Enable','off');
    set(handles.edit_steery,'Enable','off');
    % Parameters of Array Focusing
    set(handles.cb_focusing,'Enable','off');
    set(handles.pop_focus,'Enable','off');
    set(handles.edit_focal,'Enable','off');
    % Parameters of single transducer focusing.
    %set(handles.cb_single_focusing,'Enable','off');
    set(handles.pop_single_focus,'Enable','off');
    set(handles.edit_single_focal,'Enable','off');    
    % Size of Transducer/Element
    H=handles.pop_form_trans;
    val = get(H,'Value');
    if val == 1
      Stype='Line';    
    elseif val==2 
      Stype='rectangular';
    elseif val==3
      Stype='Frectangular';    
    elseif val==4
      Stype='circular';
    elseif val==5
      Stype='Fcircular';
    elseif val==6
      Stype='cir_concave';
    elseif val==7
      Stype='cir_convex';
    elseif val==8
      Stype='cyl_concave';
    elseif val==9
      Stype='cyl_convex';
    end    
    switch Stype
      case 'Line'
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','a');
	set(handles.text_T_sizeru,'Visible','on');
	
	set(handles.edit_T_sizef,'Visible','off');
	set(handles.text_T_sizef,'Visible','off');
	%set(handles.text_T_sizef,'String','xsmax');
	set(handles.text_T_sizefu,'Visible','off');
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');
	
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
	
      case {'rectangular'}
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','a');
	set(handles.text_T_sizeru,'Visible','on');
	
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','b');
	set(handles.text_T_sizefu,'Visible','on');
	
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	%set(handles.text_T_sizea,'String','ysmin');
	set(handles.text_T_sizeau,'Visible','off');
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	%set(handles.text_T_sizeb,'String','ysmax');
	set(handles.text_T_sizebu,'Visible','off');
	
      case {'Frectangular'}
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','a');
	set(handles.text_T_sizeru,'Visible','on');
	
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','b');
	set(handles.text_T_sizefu,'Visible','on');
	
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	%set(handles.text_T_sizea,'String','R');
	set(handles.text_T_sizeau,'Visible','off');

	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	%set(handles.text_T_sizeb,'String','ysmax');
	set(handles.text_T_sizebu,'Visible','off');

	% Parameters of single transducer focusing.
	%set(handles.text_single_headline_focus,'Visible','on');
	set(handles.text_single_focal,'Visible','on');
	set(handles.text_single_focalu,'Visible','on');
	set(handles.pop_single_focus,'Enable','on');
	set(handles.edit_single_focal,'Enable','on');    
      
      case {'circular'}
	set(handles.text_T_sizer,'String','r');
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizeru,'Visible','on');

	set(handles.edit_T_sizef,'Visible','off');
	set(handles.text_T_sizef,'Visible','off');
	%set(handles.text_T_sizef,'String','R');
	set(handles.text_T_sizefu,'Visible','off');
	
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');
	
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
	
      case {'Fcircular'}
	set(handles.text_T_sizer,'String','r');
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizeru,'Visible','on');

	set(handles.edit_T_sizef,'Visible','off');
	set(handles.text_T_sizef,'Visible','off');
	%set(handles.text_T_sizef,'String','R');
	set(handles.text_T_sizefu,'Visible','off');
	
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');

	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
	
	% Parameters of single transducer focusing.
	%set(handles.text_single_headline_focus,'Visible','on');
	set(handles.text_single_focal,'Visible','on');
	set(handles.text_single_focalu,'Visible','on');
	set(handles.pop_single_focus,'Enable','on');
	set(handles.edit_single_focal,'Enable','on');    
      
      case {'cir_concave','cir_convex'}
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','r');

	set(handles.text_T_sizeru,'Visible','on');
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','R');
	set(handles.text_T_sizefu,'Visible','on');
	
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');

	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
      
      case {'cyl_concave','cyl_convex'}
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','a');
	set(handles.text_T_sizeru,'Visible','on');
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','b');
	set(handles.text_T_sizefu,'Visible','on');
	set(handles.edit_T_sizea,'Visible','on');
	set(handles.text_T_sizea,'Visible','on');
	set(handles.text_T_sizea,'String','R');
	set(handles.text_T_sizeau,'Visible','on');
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
    end

  case 'Array'    
    
    % Turn off single transducer focusing.
    %set(handles.text_single_focal,'Visible','on');
    %set(handles.text_single_focalu,'Visible','on');
    set(handles.pop_single_focus,'Enable','off');
    set(handles.edit_single_focal,'Enable','off');  
	
    % Type of Transducer.
    set(handles.pop_form_trans,'Enable','off');
    %set(handles.text_form_trans,'Enable','off');
    set(handles.pop_form_elem,'Enable','on');
    %set(handles.text_form_elem,'Enable','on');
    % Grid parameters.
    set(handles.cb_grid1,'Enable','on');
    set(handles.cb_grid2,'Enable','on');
    set(handles.edit_G_Dx,'Enable','on');
    set(handles.edit_G_Dy,'Enable','on');
    set(handles.edit_G_Nx,'Enable','on');
    set(handles.edit_G_Ny,'Enable','on');
    set(handles.edit_G_filename,'Enable','off');
    % Focusing/Beam Steering.
    set(handles.cb_focusing,'Enable','on');
    set(handles.cb_steering,'Enable','on');
    % Weighting Function.
    set(handles.cb_weight1,'Enable','on');
    set(handles.cb_weight2,'Enable','on');
    % Size of Transducer/Element.
    H=handles.pop_form_elem;
    val = get(H,'Value');
    if val == 1
      Etype='E_rect';
    elseif val==2 
      Etype='E_cir';
    elseif val==3
      Etype='E_cyl_concave';
    elseif val==4    
      Etype='E_cyl_convex';
    elseif val==5
      Etype='E_Annular';
    end
    switch Etype
      case 'E_rect'
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','a');
	set(handles.text_T_sizeru,'Visible','on');
	
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','b');
	set(handles.text_T_sizefu,'Visible','on');
	
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');
	
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
	
      case 'E_cir'

	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','r');
	set(handles.text_T_sizeru,'Visible','on');
	set(handles.edit_T_sizef,'Visible','off');
	set(handles.text_T_sizef,'Visible','off');
	set(handles.text_T_sizefu,'Visible','off');
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
      
      case {'E_cyl_concave','E_cyl_convex'}
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','a');
	set(handles.text_T_sizeru,'Visible','on');
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','b');
	set(handles.text_T_sizefu,'Visible','on');
	set(handles.edit_T_sizea,'Visible','on');
	set(handles.text_T_sizea,'Visible','on');
	set(handles.text_T_sizea,'String','R');
	set(handles.text_T_sizeau,'Visible','on');
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
	
      case 'E_Annular'
	% Only on/off focusing for Annular array.
	%set(handles.pop_focus,'Enable','off'); 
	
	% No beamsteering for Annualar array.
	set(handles.cb_steering,'Enable','off'); 
                                                 
    end

    %Definition of Element Grid
    if ~strcmp(Etype,'E_Annular')
      if(get(handles.cb_grid2,'Value')==1)
	set(handles.edit_G_filename,'Enable','off');
	set(handles.edit_G_Dx,'Visible','on');
	set(handles.text_G_Dx,'Visible','on');
	set(handles.text_G_Dx,'String','Dx');
	set(handles.text_G_Dxu,'Visible','on');

	set(handles.edit_G_Dy,'Visible','on');
	set(handles.text_G_Dy,'Visible','on');
	set(handles.text_G_Dy,'String','Dy');
	set(handles.text_G_Dyu,'Visible','on');

	set(handles.edit_G_Nx,'Visible','on');
	set(handles.text_G_Nx,'Visible','on');
	set(handles.text_G_Nx,'String','Nx');

	set(handles.edit_G_Ny,'Visible','on');
	set(handles.text_G_Ny,'Visible','on');
	set(handles.text_G_Ny,'String','Ny');          
      else
	set(handles.edit_G_filename,'Enable','on');
	set(handles.edit_G_Dx,'Visible','off');
	set(handles.text_G_Dx,'Visible','off');
	set(handles.text_G_Dxu,'Visible','off');
	set(handles.edit_G_Dy,'Visible','off');
	set(handles.text_G_Dy,'Visible','off');
	set(handles.text_G_Dyu,'Visible','off');
	set(handles.edit_G_Nx,'Visible','off');
	set(handles.text_G_Nx,'Visible','off');
	set(handles.edit_G_Ny,'Visible','off');
	set(handles.text_G_Ny,'Visible','off');
      end
    
    else

      if(get(handles.cb_grid2,'Value')==1)
	set(handles.edit_G_filename,'Enable','off');
	set(handles.edit_G_Dx,'Visible','off');
	set(handles.text_G_Dx,'Visible','off');
	set(handles.text_G_Dxu,'Visible','off');
	set(handles.edit_G_Dy,'Visible','off');
	set(handles.text_G_Dy,'Visible','off');
	set(handles.text_G_Dyu,'Visible','off');
	set(handles.edit_G_Nx,'Visible','on');
	set(handles.text_G_Nx,'Visible','on');
	set(handles.text_G_Nx,'String','N');               
	set(handles.edit_G_Ny,'Visible','off');
	set(handles.text_G_Ny,'Visible','off');
	set(handles.edit_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'Visible','on');
	set(handles.text_T_sizer,'String','R0');
	set(handles.text_T_sizeru,'Visible','on');
	set(handles.edit_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'Visible','on');
	set(handles.text_T_sizef,'String','W');
	set(handles.text_T_sizefu,'Visible','on');
	set(handles.edit_T_sizea,'Visible','on');
	set(handles.text_T_sizea,'Visible','on');
	set(handles.text_T_sizea,'String','d');
	set(handles.text_T_sizeau,'Visible','on');
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
      else
	set(handles.edit_G_filename,'Enable','on');
	set(handles.edit_G_Dx,'Visible','off');
	set(handles.text_G_Dx,'Visible','off');
	set(handles.text_G_Dxu,'Visible','off');
	set(handles.edit_G_Dy,'Visible','off');
	set(handles.text_G_Dy,'Visible','off');
	set(handles.text_G_Dyu,'Visible','off');
	set(handles.edit_G_Nx,'Visible','off');
	set(handles.text_G_Nx,'Visible','off');
	set(handles.edit_G_Ny,'Visible','off');
	set(handles.text_G_Ny,'Visible','off');
	set(handles.edit_T_sizer,'Visible','off');
	set(handles.text_T_sizer,'Visible','off');
	set(handles.text_T_sizeru,'Visible','off');
	set(handles.edit_T_sizef,'Visible','off');
	set(handles.text_T_sizef,'Visible','off');
	set(handles.text_T_sizefu,'Visible','off');
	set(handles.edit_T_sizea,'Visible','off');
	set(handles.text_T_sizea,'Visible','off');
	set(handles.text_T_sizeau,'Visible','off');
	set(handles.edit_T_sizeb,'Visible','off');
	set(handles.text_T_sizeb,'Visible','off');
	set(handles.text_T_sizebu,'Visible','off');
      end
    end
    %Observation Points
    dream_gui_opoint_type(handles)
end
dream_gui_display_transducer(handles,ttype);
