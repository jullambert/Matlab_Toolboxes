function sdata = dream_gui_spectrum(handles,data,param)
% sdata = dream_gui_spectrum(handles,data,param)
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

ha=data;
dt=param{19};
dens=param{17};
[nt,nv]=size(ha);
fs=1/dt;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%Create a dialog box to input fo.%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%prompt  = {'Enter the frequency of interset (MHz):'};
%titlee   = 'Input parameters';
%lines= 1;
%def     = {'1'};
%ans  = inputdlg(prompt,titlee,lines,def);
%fo=ans{1};
%fo=str2num(fo);

%Compute pressure response if needed
ipress=get(handles.cb_pressure,'Value');
if ipress==1,
    delta(1)=1/dt;delta(2)= -1/dt; for i=1:nv,ha(:,i)=convn(ha(:,i)',delta,'same')';end
end
%Compute sepctra
hw=fft(ha,nt);hw=abs(hw);
%Plot spectra
if size(hw,2)==1
    H=figure;
    set(H,'Name',['Untitled']);
    set(H,'NumberTitle','off');
    hw=hw./max(max(hw));
    x=[0:nt-1]*fs/(nt-1);
    plot(x,hw)
    axis([0 fs/2 0 1])
    if ipress==1
       title('Magnitude Spectra of Impulse Response (Normalized)') 
    else
       title('Magnitude Spectra of Pressure Response (Normalized)')
    end
    xlabel('Frequency [MHz]')
    ylabel('Spectrum amplitude H(O,w)')
    grid
 else
     H=figure;
     set(H,'Name',['Untitled']);
     set(H,'NumberTitle','off');
     if param{11}==2
         xmin=param{24}(1);
         xmax=param{24}(2);
     elseif param{11}==3
         xmin=param{25}(1);
         xmax=param{25}(2);
     elseif param{11}==4
         xmin=param{26}(1);
         xmax=param{26}(2);
     end    
     obse_n=param{27};
     step=(xmax-xmin)/(obse_n-1);
     x=xmin:step:xmax;
     x=x(1:obse_n);
     hw=hw./max(max(hw));
     y=[0:nt-1]*fs/(nt-1);y=y(1:nt/2+1);
     mesh(x,y,hw(1:nt/2+1,:))
     axis tight
     if ipress==0
        title('Magnitude Spectra of Impulse Response (Normalized)')
     else
        title('Magnitude Spectra of Pressure Response (Normalized)')
     end
     ylabel('Frequency [MHz]')
     zlabel('Spectrum amplitude H(O,w)')
     if param{11}==2
         xlabel('Observation points(X) [mm]');
     elseif param{11}==3
         xlabel('Observation points(Y) [mm]')
     elseif param{11}==4 
         xlabel('Observation points(Z) [mm]')
     end
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
     %%%%%plot spectrum for given frequency%%%%%%%%%%%%
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %nfo=round(fo/fs*nt);
     %fo=nfo/nt*fs;
     %spfo=hw(nfo,:);
     %H=figure;
     %set(H,'Name',['Untitled']);
     %set(H,'NumberTitle','off');
     %plot(x,spfo)
     %axis tight
     %title('Spectra of Interest')
     %if param{11}==2
     %    xlabel('Observation points(X) [mm]');
     %elseif param{11}==3
     %    xlabel('Observation points(Y) [mm]')
     %elseif param{11}==4 
     %    xlabel('Observation points(Z) [mm]')
     %end
     %ylabel('H(O,w = w0)')
     %grid 
     %%%%Show info which indicate the actual used frequency of interest
     %msgbox(fo,'Freq. of Interest')
     
 end




