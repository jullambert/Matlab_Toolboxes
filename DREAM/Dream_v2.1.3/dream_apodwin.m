function [w] = dream_apodwin(apod_met,a,win_par)
% [w] = dream_apodwin(apod_met,apod,win_par)
%
% DREAM_APODWIN - Computes apodization using the same method
%        as used in the array functions in the DREAM Toolbox.
%
% Input parameters:
%
% apod_met - apodization method.
%   options:
%       'off'      - No apodization.
%       'ud'       - Used defined apodization. 
%       'triangle' - Triangle window.
%       'gauss'    - Gaussian (bell-shaped) window.
%       'raised'   - Raised cosine
%       'simply'   - Simply supported
%       'clamped'  - Clamped.
%
% apod - Is the sample length of the apodization window (or a vector
%            of apodization weights for the 'ud' option).
%
% win_par (scalar) - Parameter for raised cosine and Gaussian apodization functions.
%
% dream_apodwin is a MEX-function that is a part of the DREAM
% Toolbox available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2003,2008,2009 Fredrik Lingvall.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
