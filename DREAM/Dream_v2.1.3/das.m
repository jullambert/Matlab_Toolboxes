function [D,err] = das(Ro,s_par,delay,m_par,err_level)
% [D,err] = das(Ro,s_par,delay,m_par,err_level)
%
% DAS - Computes delays similar to the delay-and-sum algorithm. 
%  
%
% Observation point(s) ([mm]):
% Ro = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN zoN]; 
%      (N is the number of observation points).
%
% s_par = [dt nt];
%   dt [us] - Temporal discretization period (= 1/sampling freq).
%   nt      - Length of impulse response vector.
%
% Start point of SIR:
%   delay [us] - Scalar delay for all observation points or
%                a vector with individual delays for each observation point.
%
% Material parameters:
% m_par = [cp];
%   cp   [m/s] - Sound velocity.
%
% Error Handling:
% err_level : Optional parameter. err_level is text string for
% controlling the error behavior, options are:
%   'ignore' - An error is ignored (no error message is printed and
%              the program is not stopped) but the err output
%              argument is negative if an error occured.
%   'warn'   - An error message is printed but the program in not
%              stopped (and err is negative).
%   'stop'   - An error message is printed and the program is stopped.
%
% das is a MEX-function.that is a part of the DREAM Toolbox
% available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2008,2009 Fredrik Lingvall

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
