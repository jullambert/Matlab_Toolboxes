function [Y] = fftconv_p(A,B,n_cpus)
% fftconv_p(A,B,n_cpus,wisdom_str_in);
%
% FFTCONV_P - Computes the one dimensional convolution of the
% columns in matrix A and the matrix (or vector) B.
%
% Input parameters:
%
% `A'
%      An M x N matrix.
%
% `B'
%      A K x N matrix or a K-length vector. If B is a vector each
%      column in A is convolved with the vector B.
%
% `n_cpus'
%      Number of threads to use. n_cpus must be greater or equal to
%      1.
%
% `wisdom_str_in'
%      Optional parameter. If the wisdom_str_in parameter is not
%      supplied then `fftconv_p' calls fftw wisdom plan functions
%      before performing any frequency domain operations. This
%      overhead can be avoided by supplying a pre-computed fftw
%      wisdom string wisdom_str_in. For more information see the
%      fftw user manunal available at `http://www.fftw.org'.
%
% Output parameters:
%
% `Y'
%      The (M+K-1) x N output matrix.
%
% `wisdom_str_out'
%      Optional parameter. If the wisdom_str_out output parameter is
%      supplied then `fftconv_p' will call fftw wisdom plan
%      functions and return the wisdom string which then can be used
%      to speed up subsequent calls to `fftconv_p' by suppying the
%      string as the input argument wisdom_str_in.
%
% NOTE: fftconv_p requires the FFTW library version 3
% `http://www.fftw.org'.
%
% fftconv_p is a part of the DREAM Toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2006,2008,2009 Fredrik Lingvall.
%
% See also: conv_p, fftconv, conv, fftw_wisdom.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
