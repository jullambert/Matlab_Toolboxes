% DREAM (Discrete REpresentation Array Modeling) Toolbox.
% Version 2.1.0 
%
%
% Single Element Transducers:
%   dreamline     - Strip (line) transducer.
%   dreamrect     - Rectangular transducer
%   dreamrect_f   - Focused rectangular transducer.
%   dreamcirc     - Circular transducer.
%   dreamcirc_f	  - Focused circular transducer.
%   dreamspher_f  - Spherical concave transducer (focused).
%   dreamspher_d  - Spherical convex transducer (defocused).
%   dreamcylind_f - Cylindrical concave transducer (focused).
%   dreamcylind_d - Cylindrical convex transducer (defocused).
%
% Array Transducers:
%   dream_arr_rect     - Array with rectangular elements.
%   dream_arr_circ     - Array with circular elements.
%   dream_arr_cylind_f - Array with cylindrical concave elements (focused).
%   dream_arr_cylind_d - Array with cylindrical convex elements (defocused).
%   dream_arr_annu     - Annular array.
%
% Transducer/Array functions with parallel (thread) support:
%   dreamline_p     - Strip (line) transducer.
%   dreamrect_p     - Rectangular transducer
%   dreamrect_f_p   - Focused rectangular transducer.
%   dreamcirc_p     - Circular transducer.
%   dreamcirc_f__p  - Focused circular transducer.
%   dreamspher_f_p  - Spherical concave transducer (focused).
%   dreamspher_d_p  - Spherical convex transducer (defocused).
%   dreamcylind_f_p - Cylindrical concave transducer (focused).
%   dreamcylind_d_p - Cylindrical convex transducer (defocused).
%   dream_arr_rect_p     - Array with rectangular elements.
%   dream_arr_circ_p     - Array with circular elements.
%   dream_arr_cylind_f_p - Array with cylindrical concave elements (focused).
%   dream_arr_cylind_d_p - Array with cylindrical convex elements (defocused).
%   dream_arr_annu_p     - Annular array.
%
% Graphical User Interface:
%   dream_gui    - Starts the DREAM GUI (Matlab only).
%
% Misc Functions:
%   dream_apodwin - Computes apodization weights.
%   dream_att     - Computes attenuation impulse response(s).
%   conv_p        - Computes the one dimensional convolution of two
%                   matrices. 
%   fftconv_p     - Computes the one dimensional convolution of two 
%                   matrices using a fft based algorithm. 
%   sum_fftconv_p - For computing array responses using arbitrary
%                   input signals using a fft based algorithm.
%   copy_p        - Threaded in-place matrix copy.
%   saft          - Synthtic Aperture Focusing Technique (SAFT).
%   saft_p        - Synthtic Aperture Focusing Technique (SAFT),
%                   parallel version.
%   das           - Computes the delay matrix for single element 
%                   tranducers.
%   das_arr       - Computes the delay matrix for array tranducers.
%   circ_sir      - Time-continous sir for a circular transducer.
%   rect_sir      - Time-continous sir for a rectangular
%                   transducer.
%   scirc_sir     - Sampled time-continous sir for a circular transducer.
%
%  The DREAM toolbox is available at:
% `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright 2008,2009 (C) Fredrik Lingvall.

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
