function [H] = dream_att(Ro,s_par,delay,m_par)
% [H] = dream_att(Ro,s_par,delay,m_par)
%
% DREAM_ATT - Computes attenuation impulse response(s) using the same method
%    as the transducer functions in the DREAM Toolbox.
%
% Observation point(s) ([mm]).
% Ro = [xo1 yo1 zo2; xo2 yo2 zo2; ... xoN yoN zoN]; 
%      (N is the number of observation points).
%
%   s_par = [dt,nt];
%     dt [us] - Temporal discretization period  (= 1/sampling freq).
%     nt      - Length of impulse respons(s).
%
% Material parameters:
% m_par = [v cp alfa];
%   v    [m/s]      - Normal velocity.
%   cp   [m/s]      - Sound velocity.
%   alfa [dB/(cm MHz)] - Attenuation coefficient.
%
% dream_att is a MEX-function that is a part of the DREAM Toolbox
% available at `http://www.signal.uu.se/Toolbox/dream/'.
%
% Copyright (C) 2003,2008,2009 Fredrik Lingvall

% $Revision: 565 $ $Date: 2009-09-17 22:24:06 +0200 (Thu, 17 Sep 2009) $ $LastChangedBy: dream $
