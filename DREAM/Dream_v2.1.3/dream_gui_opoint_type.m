function dream_gui_opoint_type(handles)
% dream_gui_opoint_type(handles)
%
% This function is a part of DREAM toolbox available at
% `http://www.signal.uu.se/Toolbox/dream/'. 
%
% Copyright (C) 2003-2008 Fredrik Lingvall.

H=handles.pop_observe;
val = get(H,'Value');
     if val == 1
         Otype='Point';
     elseif val == 2 
         Otype='LineX';
     elseif val== 3
         Otype='LineY';
     else
         Otype='LineZ';
     end
     
switch Otype
case 'Point'
    set(handles.text_observex,'String','X0');
    set(handles.text_observey,'String','Y0');
    set(handles.text_observez,'String','Z0');
    set(handles.edit_observen,'Visible','off');    
    set(handles.text_observen,'Visible','off');
case 'LineX'    
    set(handles.text_observex,'String','X0min,X0max');
    set(handles.text_observey,'String','Y0');
    set(handles.text_observez,'String','Z0');
    set(handles.edit_observen,'Visible','on');    
    set(handles.text_observen,'Visible','on');
case 'LineY'
    set(handles.text_observex,'String','X0');
    set(handles.text_observey,'String','Y0min,Y0max');
    set(handles.text_observez,'String','Z0');
    set(handles.edit_observen,'Visible','on');    
    set(handles.text_observen,'Visible','on');
case 'LineZ'
    set(handles.text_observex,'String','X0');
    set(handles.text_observey,'String','Y0');
    set(handles.text_observez,'String','Z0min,Z0max');
    set(handles.edit_observen,'Visible','on');    
    set(handles.text_observen,'Visible','on');
    
otherwise
    return
end
