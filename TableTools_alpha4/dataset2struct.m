function s = dataset2struct(ds)
% DATASET2STRUCT  Convert dataset to standard structure (i.e.: no object inside).
%    S = DATASET2STRUCT(DS)  converts dataset DS to a standard structure.  
%    Columns which are categorical arrays are converted to cell array of 
%    strings.  Other columns become structure fields with no modification.

if ~isa(ds,'dataset')
    if istruct(ds) % already a structure?..
        s = ds;    % ..pass it as it is
        return % <===!!!
    else
        error('Input argument must be a dataset.')
    end
end

colnames = fieldnames(ds);
s = struct;
for j = 1:length(colnames)
    if isa(ds.(colnames{j}),'categorical')
        s.(colnames{j}) = cellstr(ds.(colnames{j}));
    else
        s.(colnames{j}) = ds.(colnames{j});
    end
end