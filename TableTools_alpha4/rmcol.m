function ds = rmcol(ds,col)
% RMCOL  Remove columns from a dataset array.
%    DS = RMCOL(DS,'column')  removes the specified column from the 
%    dataset DS.
%
%    S = RMCOL(DS,COLUMNS)  removes more than one column at a time
%    when COLUMNS is a cell array of strings.


%% Get Colunn Indexes (jj)
allcols = fieldnames(ds);


if ischar(col)
    jj = strmatch(col,allcols,'exact');
    
elseif iscell(col)
    jj = zeros(1,length(col));
    for i = 1:length(col)
        jj(i) = strmatch(col{i},allcols,'exact');
    end
    
elseif isnumeric(col)
    jj = col;
    
elseif islogical(col) && length(col)==size(ds,2)
    jj = find(col);
    
else error('Invalid ''column'' argument.')
    
end

%% Remove Columns
ds(:,jj) = [];

