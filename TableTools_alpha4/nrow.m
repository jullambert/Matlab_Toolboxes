function m = nrow(T)
% NROW  Number of rows of a table.
%    NROW(T)  gives the number of rows in table T.  See TABLESIZE for details.
%
% See also: NCOL, TABLESIZE.

% Programmer's Note: Using R's names: nrow, ncol (and not nrows, ncols).

[m,n] = tablesize(T);