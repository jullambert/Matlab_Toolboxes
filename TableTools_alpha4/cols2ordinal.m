function T = cols2ordinal(T,order)
% COLS2ORDINAL  Convert columns from cell array of strings to ordinal array. <todo: revoir>
%    DS = COLS2ORDINAL(DS)  converts to ordinal columns of the dataset DS, 
%    which are cell arrays of strings (i.e.: iscellstr() returns true).
%    Other columns are left unchanged.
%
%    S = COLS2ORDINAL(S)  does the same for fields of structure S.
%
% See also COLS2NOMINAL, COLS2CELLSTR.

f = @ordinal; % <only thing wich change with the sister cols2* function>

% Convert cell string columns:
cols = colnames(T);
for j = 1:length(cols)
    colname = cols{j};
    if iscellstr(T.(colname))
        T.(colname) = f(T.(colname));
    end
end