function ds = struct2dataset(s)
% STRUCT2DATASET  Convert structure to dataset, converting string-cell columns to nominal. <Alpha version. Could change!>
%    DS = STRUCT2DATASET(S)

if ~exist('dataset')
    error('Cannot find Statistics Toolbox. You need it installed before to continue.')
end

% Structure -> Dataset:
ds = dataset(s);

% Convert string-cell columns to nominal:
ds = cols2nominal(ds);
