function S = converttext(S,newtype)
% CONVERTTEXT  Conversion between string matrix, cell array of strings, categorical array.
%    Text data (e.g.: list of words) in Matlab can be stored in three different 
%    types of data structures:
%    - old school Matlab4-like string matrix ('char')
%    - cell array of strings ('cellstr')
%    - Stats toolbox�s categorical array ('nominal' or 'ordinal')
%
%    CONVERTTEXT(S,NEWTYPE)  converts text data S of any type (see above)  
%    to NEWTYPE.  NEWTYPE can be 'char', 'cellstr', 'nominal' or 'ordinal'.
%    If S is not text data (see ISTEXT to check), it is returned unmodified.
%
% See also: ISTEXT, CONVERTTABLE, CHAR, CELLSTR, NOMINAL, ORDINAL.

if istext(S)
    switch newtype
        case 'char',    S = char(S);
        case 'cellstr', S = cellstr(S);
        case 'nominal', S = nominal(S);
        case 'ordinal', S = ordinal(S);
        otherwise
            error(['Invalid value for ''NEWTYPE'' argument.' 10 ...
                'Valid values are: ''char'', ''cellstr'', ''nominal'', ''ordinal''.'])
    end
end