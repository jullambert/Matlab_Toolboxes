function [b,e] = istabular(T)
% ISTABULAR  True for tables, datasets, tabular structures.
%    ISTABULAR(T)  returns true if T is a 
%        - table
%        - dataset
%        - scalar structure with fields having the same number of rows
%        - non-scalare structure with fields having no more than one row
%
%    [b,err] = ISTABULAR(T)  returns also an error message (the reason why 
%    it's not tabular.)
%
% Ben Jacob, Jul 2015.

switch class(T)
    case 'table'
        b = true;
        e = '';
        
    case 'dataset'
        b = true;
        e = '';
        
    case 'struct'
        try struct2dataset(T)
            b = true;
            e = '';
        catch
            b = false;
            e = lasterr;
            f = find(e==10);
            e = e(f(1)+1:end);
        end
        
    otherwise
        b = false;
        e = 'Invalid type';
end
            