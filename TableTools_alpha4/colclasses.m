function classes = colclasses(DS);
% COLCLASSES  Get dataset column classes.
%    CLASSES = COLCLASSES(DS)  returns a cell array containing
%    the name of the class of each column of the dataset DS.
%
%    CLASSES = COLCLASSES(S)  returns a cell array containing
%    the name of the class of each field of the structure S.
%
% See also COLNAMES.

% datasetfun(@class,DS); miserably crashes on Matlab 7.8.
% => let's write a loop...
fields = colnames(DS);
classes = cell(size(fields));
for j = 1:length(fields)
    classes{j} = class(DS.(fields{j}));
end