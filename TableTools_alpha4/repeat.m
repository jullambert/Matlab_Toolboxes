function w = repeat(v,n) 
% REPEAT  Replicates elements of a vector.
%    w = REPEAT(v,n)  repeats elements in vector v n times.  n can be a scalar  
%    or a vector of the same size than v.
%
%    The reverse of this function is UNREPEAT.
%
%    See also: UNREPEAT, REPROWS.
%
% Ben Jacob, Jan 2015.

if isscalar(n) == 1
    n = n + zeros(size(v));
elseif length(n) ~= length(v)
    error('Inconsistent arguments dimensions.')
end

w = [];
for i = 1:length(n)
    w = [w; repmat(v(i),n(i),1)];
end
if size(v,2) > size(v,1)
    w = w';
end