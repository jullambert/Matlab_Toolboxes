function [m,n] = tablesize(T,dim)
% TABLESIZE  Size of table.
%    [M,N] = TABLESIZE(S), where S is a structure whose fields should be  
%    column-vectors of the same size,  returns M: the number of rows in the  
%    first field and N: the number of fields.
%
%    [M,N] = TABLESIZE(X), where X is anything else, is the same 
%    than [M,N] = SIZE(X).
%
%    TABLESIZE(X,DIM)  returns the number of rows (DIM=1) or columns (DIM=2).
%
% See also: NROW, NCOL.

if isstruct(T) % Structure:
    fields = fieldnames(T);
    if isempty(fields)
        m = 0;
        n = 0;
    else
        m = size(T.(fields{1}),1);
        n = length(fields);
    end
else           % Any other type:
    [m,n] = size(T);   
end

if nargin > 1
    switch dim 
        case 1, %(nothing to do)
        case 2, m = n;
        otherwise error('Invalid input value. ''DIM'' argument should be 1 or 2.')
    end
    clear n
end