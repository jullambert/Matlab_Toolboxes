function n = ncol(T)
% NCOL  Number of cols of a table.
%    NCOL(T)  gives the number of cols in table T.  See TABLESIZE for details.
%
% See also: NROW, TABLESIZE.

% Programmer's Note: Using R's names: nrow, ncol (and not nrows, ncols).

[m,n] = tablesize(T);