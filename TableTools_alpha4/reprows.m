function A = reprows(A,nRep)
% REPROWS  Replicates rows.
%    REPROWS(A,nRep)  replicates rows of array A nRep times.  A is an array of any type.
%    nRep can also be a vector containing one number per row in A.
%
%    Examples: 
%         M = magic(3);
% 
%         M =
% 
%              8     1     6
%              3     5     7
%              4     9     2
% 
%         reprows(M,2)
% 
%         ans =
% 
%              8     1     6
%              8     1     6
%              3     5     7
%              3     5     7
%              4     9     2
%              4     9     2
% 
%         reprows(M,[2 1 3])
% 
%         ans =
% 
%              8     1     6
%              8     1     6
%              3     5     7
%              4     9     2
%              4     9     2
%              4     9     2

if length(nRep) == 1
    nRep = repmat(nRep,size(A,1),1);
elseif length(nRep) ~= size(A,1)
    error('Inconsistent arguments dimensions.')
end

ii = [];
for i = 1:length(nRep)
    ii = [ii; repmat(i,nRep(i),1)];
end

A = A(ii,:);
