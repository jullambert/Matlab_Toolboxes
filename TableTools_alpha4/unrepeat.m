function [v,n] = unrepeat(w) 
% UNREPEAT  Suppress (and count) repetitions. Reverse of repeat().
%    [v,n] = UNREPEAT(w)  returns vector v, with same elements than w but without 
%    the repetitions, and vector n, the number of repetitions for each element.
%
%    The reverse of this function is REPEAT.
%
%    Example:
%             [v,n] = unrepeat([1 1 1 2 3 3 1])
% 
%             v =
% 
%                  1     2     3     1
% 
%             n =
% 
%                  3     1     2     1
%
%    See also: REPEAT, REPROWS.
%
% Ben Jacob, Jan 2015.

tokeep = find([1 diff(w)]);

v = w(tokeep);
n = diff([tokeep length(w)+1]);