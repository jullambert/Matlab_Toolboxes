function long_ds = melttable(ds,varargin)
% MELTTABLE  Melt columns of a table/dataset to get a long-format table/dataset.
%    MELTTABLE(T) <TODO>
%    MELTTABLE(T, GroupingVariables)
%    MELTTABLE(T, GroupingVariables, MEASURE_VARS)
%    MELTTABLE(..., 'PARAM1',val1, 'PARAM2',val2, ...)
%
% Ben Jacob, Sep 2014.
%
% Doc of original R's melt() function:
%    http://cran.r-project.org/web/packages/reshape2/reshape2.pdf
%    see melt.data.frame(), p.8
%
% See also: CASTTABLE.


%% Input Args
col_names = fieldnames(ds);

% Defaults:
id_vars = {}; % will be defined below
measure_vars = {}; % id.
variable_name = 'variable';
value_name = 'value';
nan_rm = true;

% Parse varargin:
if  ~isempty(varargin) && (~ischar(varargin{1}) || ismember(varargin{1},col_names))
    id_idx = get_col_indexes(ds,varargin{1});
    id_vars = col_names(id_idx);
    varargin(1) = [];
    if  ~isempty(varargin) && (~ischar(varargin{1}) || ismember(varargin{1},col_names))
        measure_idx = get_col_indexes(ds,varargin{1});
        measure_vars = col_names(measure_idx);
        varargin(1) = [];
    end
end
for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'id-vars'
            id_vars = varargin{i+1};
            id_idx = get_col_indexes(ds,id_vars);
        case 'measure-vars'
            measure_vars = varargin{i+1};
            measure_idx = get_col_indexes(ds,measure_vars);
        case 'variable-name'
            variable_name = varargin{i+1};
        case 'value-name'
            value_name = varargin{i+1};
        case 'value-type' 
            %<TODO>
        case 'nan-rm'
            nan_rm = varargin{i+1};
        otherwise 
            error(['Unknown parameter: ''' varargin{i} '''.'])
    end
end
if isempty(id_vars) && isempty(measure_vars)
    error('''id-vars'' and ''measure-vars'' not defined. You have to provide at least one of the two.')
elseif isempty(id_vars)
    id_idx = setdiff(1:size(ds,2),measure_idx);
    id_vars = col_names(id_idx);
elseif isempty(measure_vars)
    measure_idx = setdiff(1:size(ds,2),id_idx);
    measure_vars = col_names(measure_idx);
end

%% Melt Dataset
n = size(measure_idx,2);
m = size(ds,1);
id_cols = ds(:,id_idx);
ms_cols = ds(:,measure_idx);
long_ds = repmat(id_cols,n,1);
variable = nominal;
warning off stats:categorical:subsasgn:NewLevelsAdded
for i = 1:n
    variable((i-1)*m+1:i*m,1) = measure_vars{i};
    if i==1, value = ds.(measure_vars{i});
    else     value = [value; ds.(measure_vars{i})];
    end
end
long_ds.(variable_name) = variable;
long_ds.(value_name) = value;

%% Remove Rows with Missing Data
if nan_rm
    val = long_ds.(value_name);
    if isnumeric(val)
        missing = isnan(val);
    elseif isa(val,'categorical')
        missing = isundefined(val);
    else
        missing = false(size(val));
    end
    long_ds(missing,:) = [];
end


%% Sub-Funs
function idx = get_col_indexes(ds,vars)
% GET_COL_INDEXES  Convert '*_vars' input arg to column indexes.
colnames = fieldnames(ds);
if isnumeric(vars)
    idx = vars;
elseif islogical(vars)
    idx = find(vars);
elseif ischar(vars)
    idx = strmatch(vars,colnames,'exact');
    if isempty(idx), error(['No column of that name: ''' vars '''.']), end
elseif iscell(vars)
    for i = 1:length(vars)
        idx(i) = strmatch(vars{i},colnames,'exact');
        if isempty(idx), error(['No column of that name: ''' vars{i} '''.']), end
    end
end
