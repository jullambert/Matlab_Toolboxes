function names = colnames(T)
% COLNAMES  Get dataset column names.
%    NAMES = COLNAMES(T)  returns a cell array of strings containing 
%    the column names associated with the table/dataset T.
%
%    Nota bene: The difference with the FIELDNAMES function is that 
%    FIELDNAMES returns the column names, plus 'Properties' which is
%    a standard field of both table and dataset classes.
%
%    NAMES = COLNAMES(S), where S is a structure, simply calls 
%    FIELDNAMES(S).
%
% See also FIELDNAMES, COLCLASSES.

switch class(T)
    case 'table'
        names = T.Properties.VariableNames;
    case 'dataset'
        names = get(T,'VarNames');
    case 'struct'
        names = fieldnames(T)';
    otherwise 
        ('Invalid input type.  T must be a table, a dataset or a structure.')
end
