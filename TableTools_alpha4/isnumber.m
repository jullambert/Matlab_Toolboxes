function yes = isnumber(x)
% ISNUMBER  True for double, single, integer, logical. <todo: Freeze name: isnumber/isnumdata ?>
%    ISNUMBER(X)  is the same than ISNUMERIC(X) || ISLOGICAL(X).
%
% See also: ISTEXTDATA.

yes = isnumeric(x) || islogical(x);