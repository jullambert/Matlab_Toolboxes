function T = cols2cellstr(T)
% COLS2CELLSTR  Convert columns from categorical array to cell array of strings.
%    DS = COLS2CELLSTR(DS)  converts columns of the dataset DS, of the categorical 
%    class (nominal or ordinal) categorical to cell arrays of strings (i.e.: iscellstr() returns true).
%    Other columns are left unchanged.
%
%    S = COLS2CELLSTR(S)  does the same for fields of structure S.
%
% See also COLS2ORDINAL, COLS2NOMINAL.

cols = colnames(T);
for j = 1:length(cols) 
    colname = cols{j};
    if isa(T.(colname),'categorical')
        T.(colname) = cellstr(T.(colname));
    end
end