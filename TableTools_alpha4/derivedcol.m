function col = derivedcol(fun, T, inputCol, groupCols)
% DERIVEDCOL  Get new column vector by applying function to each element of column(s).
%    DERIVEDCOL(fun, T, InputCol, GroupingCols)  applies function �fun� to column InputCol 
%    of table T.
%    InputCol must be numeric, GroupingCols must be numeric or categorical. 
%
% Example:
%       T.mean = derivedcol(@mean, T, 'rt', {'cue','side'});
%       T.sd   = derivedcol(@std , T, 'rt', {'cue','side'});
%       T.outlier = T.rt < T.mean - 2.5*T.sd | T.rt > T.mean + 2.5*T.sd;
%
% Benvenuto Jacob, Jul 2015.

%% Input Args
if ~istable(T)
    error('Invalid argument: T must be a table.')
end

cols = colnames(T);
if isnumeric(inputCol),      inputCol = cols{inputCol};
end
if ischar(groupCols),        groupCols = {groupCols};
elseif isnumeric(groupCols), groupCols = cols(groupCols)%;
end

m = size(T,1);

%% Output Arg
col = zeros(m,1);

%% Main
groups = unique(T(:,groupCols));

for g = 1 : size(groups,1)
    in = true(m,1);
    for j = 1 : length(groupCols)
        gcol = groupCols{j};
        in = in & ( T.(gcol) == groups.(gcol)(g) );
    end
    col(in) = fun(T.(inputCol)(in));
end
