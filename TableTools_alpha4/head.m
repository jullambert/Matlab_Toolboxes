function h = head(T,m)
% HEAD  Display the beginning of a table.

switch nargin
    case 0, error('Missing input argument .')
    case 1, m = 10;
end

if isstruct(T)
    T = dataset(T); 
elseif ischar(T) % T is the name of a variable..
    cmd = [mfilename '(' T ')'];  % <===RECURSIVE-CALL===!!!
    evalin('base',cmd) 
    return                        % <===!!!
end

h = T(1:m,:);

if ~nargout
    disp(h)
    clear h
end