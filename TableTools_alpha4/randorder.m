function [w,order] = randorder(v,option)
% RANDORDER  Randomize order of elements.
%    [v,order] = RANDORDER(v)  ramdomizes order of elements of vector v.
%
%    [v,order] = RANDORDER(v,'norepet')  does not allows an element to 
%    be repeated in successive positions.

%% First random order
order = randperm(length(v));
w = v(order);

%% 'NoRepet' option: Successive repetitions are not allowed
if (nargin > 1) && (strcmpi(option,'norepet') || strcmpi(option,'-norepet'))
    % Redo it until we get an order without any repetition:
    while any(diff(v) == 0)
        order = randperm(length(v));
        w = v(order);
    end
end