function T = cols2categorical(T)
% COLS2CATEGORICAL  Convert columns from cell array of strings to categorical array. <todo: � revoir>
%    DS = COLS2CATEGORICAL(T)  converts to categorical columns of the table/dataset T, 
%    which are cell arrays of strings (i.e.: iscellstr() returns true).  Other columns
%    are left unchanged.  On Matlab versions earlier than R2013b, where the categorical 
%    class is final and cannot be instanciated, the nominal class will be used instead.
%
%    S = COLS2CATEGORICAL(S)  does the same for fields of structure S.
%
% See also COLS2CELLSTR.

% <TODO: change name to cellstr2categorical ???>

try 
    categorical % will succeed on Matlab R2013b+
    f = @categorical; % <only thing wich change with the sister cols2* functions>
catch
    f = @nominal;
end

% Convert cell string columns:
cols = colnames(T);
for j = 1:length(cols) 
    colname = cols{j};
    if iscellstr(T.(colname))
        T.(colname) = f(T.(colname));
    end
end