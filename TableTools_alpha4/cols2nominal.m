function T = cols2nominal(T)
% COLS2NOMINAL  Convert columns from cell array of strings to nominal array. <todo: � revoir>
%    DS = COLS2NOMINAL(DS)  converts to nominal columns of the dataset DS, 
%    which are cell arrays of strings (i.e.: iscellstr() returns true).
%    Other columns are left unchanged.
%
%    S = COLS2NOMINAL(S)  does the same for fields of structure S.
%
% See also COLS2ORDINAL, COLS2CELLSTR.

f = @nominal; % <only thing wich change with the sister cols2* function>

% Convert cell string columns:
cols = colnames(T);
for j = 1:length(cols) 
    colname = cols{j};
    if iscellstr(T.(colname))
        T.(colname) = f(T.(colname));
    end
end