function B = ismissing(X)
% ISMISSING  True for missing data. <TODO: must have the same behavior than new dataset/table eponymous method>
%    B = ISMISSING(X)  returns logical array B.  The behavior depends on
%    the type of X:
%
%    * if X is a double or single array, B is true for NaNs,
%    * if X is a categorical array, B is true for undefined cells and cells
%      containing the string 'NA' or 'NaN' (case sensitive),
%    * if X is a cell array of string (see ISCELLSTR), B is true for empty  
%      cells and cells containing the string 'NA' or 'NaN' (case sensitive),
%    * if X is an other cell array, B is true for empty cells (i.e.: cells
%      containing an empty array) and cells containing NaN or the string 'NA'
%      or 'NaN'.
%    * if X is a character array, B is a column vector with the same number
%      of rows than X, and is true for blank rows and rows containing 'NA' 
%      or 'NaN' (trailing  whitespace ignored).
%    * if X is a logical or an integer, B is always false,
%    * if X is a dataset, B has the same size than X and ISMISSING is called 
%      recursively on each column.

switch class(X)
    case {'double','single'}
        B = isnan(X);
    case {'nominal','ordinal'}
        c = cellstr(X);
        B = isundefined(X) | strcmp('NA',c) | strcmp('NaN',c);
    case 'cell'
        if iscellstr(X)
            B = strcmp('',c) | strcmp('NA',c) | strcmp('NaN',c);
        else
            B = false(size(X));
            for ind = 1:numel(X);
                B(ind) = isempty(X{ind}) || (~ischar(X{ind}) && isnan(X{ind})) ...
                    || strcmp('NA',X{ind}) || strcmp('NaN',X{ind});
            end
        end
    case 'char'
        B = all((isspace(X) | ~X)')'; % row with only withe space or null
        B(strmatch('NA',c,'exact')) = true;
        B(strmatch('NaN',c,'exact')) = true;
    case {'logical','int8','int16','int32','int64','uint8','uint16','uint32','uint64'}
        B = false(size(X));
    case 'dataset'
        B = false(size(X));
        cols = colnames(X);
        for j = 1:length(cols)
            B(:,j) = ismissing(X.(cols{j})); % <=== RECURSIVE CALL ===!!!
        end
    otherwise
        error('Invalid input type.')
end