function wide_ds = casttable(ds, x_vars, y_vars, varargin)
% CASTTABLE  An implementation of R's dcast() function.
%    CASTTABLE(T, x_vars, y_vars, varargin)
%
% Example:
%         tips = struct2dataset(readtable('tips.txt'));
% 
%         head(tips)
% 
%         mtips = melttable(tips, {'sex' 'smoker' 'day' 'time'}, {'tip' 'total_bill'});
% 
%         head(mtips)
% 
%         casttable(mtips, 'sex', 'variable', @mean)
% 
%         ans =
% 
%             sex       tip       total_bill
%             Female    2.8334    18.057
%             Male      3.0896    20.744
% 
%         casttable(mtips, {'day','time'}, 'variable', @mean)
% 
%         ans =
% 
%             day     time      tip       total_bill
%             Fri     Dinner      2.94    19.663
%             Fri     Lunch     2.3829    12.846
%             Sat     Dinner    2.9931    20.441
%             Sun     Dinner    3.2551     21.41
%             Thur    Dinner         3     18.78
%             Thur    Lunch     2.7677    17.665
% 
%         casttable(mtips, {'day','time'}, 'variable', @length)
% 
%         ans =
% 
%             day     time      tip    total_bill
%             Fri     Dinner    12     12
%             Fri     Lunch      7      7
%             Sat     Dinner    87     87
%             Sun     Dinner    76     76
%             Thur    Dinner     1      1
%             Thur    Lunch     61     61
% 
%         casttable(mtips, 'day', 'sex', @length)
% 
%         ans =
% 
%             day     Female    Male
%             Fri     18         20
%             Sat     56        118
%             Sun     36        116
%             Thur    64         60
%
% See also: MELTTABLE.



%% Input Args
col_names = fieldnames(ds);
if nargin < 3
    error('Not enough input arguments.')
end
if isempty(x_vars) && isempty(y_vars)
    error('''id-vars'' and ''measure-vars'' not defined. You have to provide at least one of the two.')
end
if ~isempty(x_vars)
    x_idx = get_col_indexes(ds,x_vars);
    x_vars = col_names(x_idx);
end
if ~isempty(y_vars)
    y_idx = get_col_indexes(ds,y_vars);
    y_vars = col_names(y_idx);
end
if ~isempty(intersect(x_idx,y_idx))
    error(['Same variable found among x_vars and y_vars.'])
end

% Parse varargin:
fun_aggregate = @length; %<TODO!>
if ~isempty(varargin) && isa(varargin{1},'function_handle')
    fun_aggregate = varargin{1};
    varargin(1) = [];
end
value_var = 'value'; %<TODO!>
for i = 1:2:length(varargin)
    switch lower(varargin{i})
        case 'value-var'
            value_name = varargin{i+1};
        case 'fun-aggregate'
            fun_aggregate = varargin{i+1};
        case 'fill' %<TODO>
            fill = varargin{i+1};
        otherwise
            error(['Unknown parameter: ''' varargin{i} '''.'])
    end
end

%% Cast Dataset
yvar = ds.(y_vars{1}); %<TODO: multiples cols>
new_cols = unique(yvar); %<TODO: multiples cols>
if isa(new_cols,'categorical')
    new_cols = getlabels(new_cols);
elseif isnumeric(new_cols)
    nums = new_cols;
    new_cols = cell(size(nums))';
    for i = 1:length(new_cols)
        new_cols{i} = [y_vars{1} num2str(nums(i))]; %<TODO: multiples cols>
    end
end
id_ds = unique(ds(:,[x_vars]));
wide_ds = id_ds;
for j = 1:length(new_cols)
    wide_ds = addcol(wide_ds, new_cols{j}, class(ds.(value_var)));
end

warned = false;
for j = 1:length(new_cols)
    col = new_cols{j};
    col_mask = ds.(y_vars{1})==col; %<TODO: multiples cols>
    for i = 1:size(id_ds,1)
        mask = col_mask;
        for j2 = 1:length(x_vars)
            mask = mask & ds.(x_vars{j2})==id_ds.(x_vars{j2})(i); %<TODO: fix it for cells>
        end
        group = ds.(value_var)(mask);
        if numel(group) > 0
            aggreg = fun_aggregate(group);
            if ~strcmp(class(wide_ds.(col)),aggreg)
                eval(sprintf('wide_ds.(col) = %s(wide_ds.(col));','double'))
            end
            wide_ds.(col)(i) = aggreg;
%             try   
%                 wide_ds.(col)(i) = fun_aggregate(group);
%             catch
%                 if ~isnumeric(wide_ds.(col))
%                     wide_ds.(col) = double(wide_ds.(col));
%                 end
%                 wide_ds.(col)(i) = fun_aggregate(double(group));
%                 if ~warned
%                     warning('Cannot apply aggregate function to nominal type; converting to double.')
%                     warned = true;
%                 end
%             end
        end
    end
end


%% Sub-Funs
function idx = get_col_indexes(ds,vars)
% GET_COL_INDEXES  Convert '*_vars' input arg to column indexes.
colnames = fieldnames(ds);
if isnumeric(vars)
    idx = vars;
elseif islogical(vars)
    idx = find(vars);
elseif ischar(vars)
    idx = strmatch(vars,colnames,'exact');
    if isempty(idx), error(['No column of that name: ''' vars '''.']), end
elseif iscell(vars)
    for i = 1:length(vars)
        idx(i) = strmatch(vars{i},colnames,'exact');
        if isempty(idx), error(['No column of that name: ''' vars{i} '''.']), end
    end
end