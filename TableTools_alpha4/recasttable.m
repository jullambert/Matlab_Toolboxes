function ds = recasttable(ds, x_vars, y_vars, aggregation_fun)
% RECASTTABLE  <unfinished>

ds = melt(ds,x_vars,y_vars);
ds = dcast(ds,x_vars,'variable',aggregation_fun);