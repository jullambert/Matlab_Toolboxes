function h = viewtable(T)
% VIEWTABLE  Edit table/dataset.
%    VIEWTABLE(T)  edits table/dataset T.
%
%    h = VIEWTABLE(T)  returns the handle of the uitable object.

switch class(T)
    case 'table',   t2c = @table2cell;
    case 'dataset', t2c = @dataset2cell; %<FIXME: not same args! FIXME: not on ML 7.5!>
    otherwise error('Invalid argument. T must be a table or a dataset.')
end

figure('Units','norm', 'Pos',[0 .05 1 .89]);
set(gcf,'Units','pixel')

h = uitable('data',t2c(T), 'ColumnName',colnames(T), ...
    'Units','norm', 'Pos',[0 0 1 1], ...
    'ColumnEditable',true, 'RearrangeableColumns','on');

p = get(gcf,'Pos');
e = get(h,'Extent');
chouilla = 24;

if e(3) < 1
    p(3) = round(p(3) * e(3)) + chouilla;
end
if e(4) < 1
    p(4) = round(p(4) * e(4));
    p(2) = round(p(4)/2); % center it
end
set(gcf,'Pos',p)
set(gcf,'Units','norm');
p = get(gcf,'POs');
p(1) = 1 - p(3);
set(gcf,'Pos',p) % align it on the right

if ~nargout
    clear h
end