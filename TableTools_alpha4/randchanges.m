function seq = randchanges(nCond,nReplic,p)
% RANDCHANGES  Random sequence following given probabilities of alternation. <deprecated: use RANDALTERN>
%    seq = RANDCHANGES(nCond,nReplic,p)
%
%    Example: 
%      nCond = 8;
%      nReplic = 4;
%      p = 1 ./ 2.^(1:nReplic); % geometric distribution
%      seq = randchanges(nCond,nReplic,p);
%
%      T = readcondtable('conditions.txt'); % get condition table: one row per condition
%      T = T(seq,:); % trial table: one row per trial

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEBUG = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Inputs
% Check arg consistency:
if (nReplic > length(p))     % p too short..
    p = [p zeros(1,nReplic-length(p))]; % ..pad it with zeros
elseif (nReplic < length(p)) % p too long..
    p = p(1:nReplic);                   % ..clip it
end
p = p / sum(p); % normalize: sum of proba = 1

% Vars:
nTrials = nCond .* nReplic;
nRepetMax = length(p);
cumDistrib = cumsum(p(end:-1:1));
cumDistrib = cumDistrib(end:-1:1);

%% Get random distrib of number of changes
nRepet = [];
while sum(nRepet) < nTrials
    nRepet(end+1) = sum(rand < cumDistrib);
end
nRepet(end) = nTrials - sum(nRepet(1:end-1));

%%
nRepetDescend = sort(nRepet,'descend');
iCondDescend = zeros(size(nRepetDescend));
nOccurences  = zeros(nCond,1); % occurences of each condition

for i = 1:length(nRepet)
    iCond = randpick(find(nRepetMax-nOccurences >= nRepetDescend(i)));
    nOccurences(iCond) = nOccurences(iCond) + nRepetDescend(i);
    iCondDescend(i) = iCond;
end

%% Randomize order
order = random_order_without_repetitions(iCondDescend);
iCondRand  = iCondDescend(order);
nRepetRand = nRepetDescend(order);

%% Final sequence
seq = [];
for i = 1:length(nRepetRand)
    seq = [seq repmat(iCondRand(i), 1, nRepetRand(i))];
end

if DEBUG
    workspaceexport;
    disp ('cond #:')
    disp(iCondRand)
    disp ('how many times:')
    disp(nRepetRand)
end


%% Sub-Functions
function ele = randpick(v)
% Pick an element randomly.
v = v(randperm(length(v)));
ele = v(1);

function order = random_order_without_repetitions(v)
% Randomize order, without moving the last one (it has been clipped just after the WHILE loop 
% above, which would induce a bias in the probabilities.):
order = [randperm(length(v)-1) length(v)];
% Redo it until we get an order without any repetition:
while any(diff(v(order)) == 0)
    order = [randperm(length(v)-1) length(v)];
end
