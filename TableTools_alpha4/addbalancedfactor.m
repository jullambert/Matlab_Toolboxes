function ds = addbalancedfactor(ds,varname,values,varargin)
% ADDBALANCEDFACTOR  <Alpha version. Everything could change!> 
%                    Add a factor to a condition table, keeping number of each condition balanced.
%
%    DS = ADDBALANCEDFACTOR(DS,varname,values)  adds a new variable (column), ignoring the existing 
%    ones when randomising the levels.
%
%    DS = ADDBALANCEDFACTOR(DS,varname,values,condvar)  adds a new variable, balancing combinations
%    with the existing levels of condvar.
%
%    DS = ADDBALANCEDFACTOR(DS,varname,values,condvar,condvalues)  See example below.
%
%    DS = ADDBALANCEDFACTOR(DS,varname,values,condvar1,condvalues1,condvar2,condvalues2,...)
%
%    Example: 
%       % Use PsychToolbox function BalanceFactors() to build a table of balanced factors:
%       [Signif,Congruency] = BalanceFactors(4, 1, {'value','size'}, {'congruent','incongruent'});
%       trials = dataset(Signif,Congruency)
% 
%             trials = 
% 
%                 Signif         Congruency       
%                 'size'         'incongruent'    
%                 'size'         'congruent'      
%                 'value'        'congruent'      
%                 'value'        'incongruent'    
%                 'size'         'congruent'      
%                 'size'         'congruent'      
%                 'value'        'congruent'      
%                 'size'         'incongruent'    
%                 'value'        'congruent'      
%                 'value'        'incongruent'    
%                 'value'        'congruent'      
%                 'size'         'incongruent'    
%                 'value'        'incongruent'    
%                 'size'         'congruent'      
%                 'size'         'incongruent'    
%                 'value'        'incongruent' 
%
%       % Just for the need of the demo, let's sort our factors:
%       trials = sortrows(trials);
%
%       % Use ADDBALANCEDFACTOR() to add a new factor, keeping the levels balanced:
%       trials = addbalancedfactor(trials, 'NumDist', 1:4, ...
%                   'Signif',{'value', 'size'}, 'Congruency',{'congruent','incongruent'})
%
%             Warning: Columns containing cell arrays are not supported.  Converting to ordinal...
%             > In addbalancedfactor at 79
% 
%             trials = 
%
%                 Signif    Congruency     NumDist
%                 size      congruent      1      
%                 size      congruent      4      
%                 size      congruent      2      
%                 size      congruent      3      
%                 size      incongruent    3      
%                 size      incongruent    4      
%                 size      incongruent    1      
%                 size      incongruent    2      
%                 value     congruent      3      
%                 value     congruent      1      
%                 value     congruent      2      
%                 value     congruent      4      
%                 value     incongruent    2      
%                 value     incongruent    3      
%                 value     incongruent    1      
%                 value     incongruent    4   
%
%    See also READCONDTABLE (*), BalanceFactors (**), BalanceTrials (**)
% 
%       (*)  CosyGraphics function
%       (**) PsychToolbox function

%% Input Args
nrows = length(ds);

if ~isa(ds,'dataset')
    error('First argument must be a dataset. See STRUCT2DATASET to convert a structure to a dataset.')
elseif ~isempty(strmatch('cell',colclasses(ds)))
    warning('Columns containing cell arrays are not supported.  Converting to ordinal...')
    ds = cellfields2ordinal(ds);
end

if nargin < 3
    error('Too few input arguments.')
    
elseif nargin == 3
    indexes = splitindexes(1:nrows,length(values));

elseif nargin == 4
    ds = addfactor(ds,varname,values,varargin{:},'*'); % <==== RECURSIVE ====!!!
    return %                                             <====== RETURN =====!!!
    
elseif mod(nargin,2) % odd number of args
    filters = true(nrows,1);
    for v = 1:2:length(varargin)-1
        condvar = varargin{v};
        condvalues = varargin{v+1};
        if ischar(condvalues)
            if strcmp(condvalues,'*')
                condvalues = unique(ds.(condvar));
            else
                condvalues = ordinal(condvalues);
            end
        end
        old_filters = filters;
        n = length(condvalues);
        filters = [];
        for j = 1:n
            val = condvalues(j);
            if iscell(val), val = val{1}; end
            try yes = ds.(condvar)==val;
            catch
                yes = false(nrows,1); % fix 'Ordinal level orientation not present.' error.
            end
            for j2 = 1:size(old_filters,2)
                filters(:,end+1) = old_filters(:,j2) & yes;
            end
        end
    end
    empty = sum(filters)==0;
    filters(:,empty) = [];
    indexes = cell(size(filters,2),length(values));
    for j = 1:size(filters,2)
        indexes(j,:) = splitindexes(find(filters(:,j)),length(values));
    end
    
else
    error('Wrong number of input arguments.')
    
end

%% Add Variable to Table
% Add Empty column:
if ~ismember(varname,colnames(ds))
    if iscell(values)
        ds.(varname) = ordinal({},values,values);
    else
        ds = addcol(ds,varname,class(values));
    end
end

% Fill it:
if iscell(values), values = ordinal(values,values,values); end
for i = 1:size(indexes,1)
    for j = 1:size(indexes,2)
        ds.(varname)(indexes{i,j}) = values(j);
    end
end


%% Sub-funs
function subsets = splitindexes(indexes,d)
% SPLITINDEXES  Split indexes into d subsets.
nn = roundslices(length(indexes),d);
indexes = indexes(randperm(length(indexes)));
behind = 0;
for i = 1:length(nn)
    subsets{i} = indexes(behind+1:behind+nn(i));
    behind = behind + nn(i);
end

function nn = roundslices(m,d)
% ROUNDSLICES  Divide m elements into d slices. (Helper fun for SPLITINDEXES.)
rest = m;
for i = 1:d
    nn(i) = round(rest/(d-i+1));
    rest = rest - nn(i);
end
nn = nn(randperm(length(nn)));
