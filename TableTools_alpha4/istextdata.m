function yes = istextdata(S)
% ISTEXTDATA  True for char array, cell array of strings, categorical array.
%    Text data (e.g.: list of words) in Matlab can be stored in three different 
%    types of data structures:
%    - old school Matlab4-like string matrix ('char')
%    - cell array of strings (see ISCELLSTR)
%    - Stats toolbox�s categorical arrays ('nominal' or 'ordinal')
%
%    ISTEXTDATA(S)  returns true for any of those types.
%
% See also: ISNUMBER, CONVERTTEXT.

yes =  ischar(S) ...
    || iscellstr(S) ...
    || isa(S,'categorical');
