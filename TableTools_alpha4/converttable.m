function T = converttable(T,tabletype,texttype)
% CONVERTTABLE <unfinished> <todo>
%    CONVERTTABLE(T, tabletype [,texttype])


%% Input Args
if ~ismember(class(T),{'dataset','struct'})
    error('Invalid input argument type: T must be a dataset or a structure of vertical vectors.')
end

%% Vars
cols = colnames(T);
[m,n] = tablesize(T);

%% Convert Table Itself
switch lower(tabletype)
    case {'d','ds','dataset'}
        if isstruct(T)
            T = dataset(T); 
        end
    case {'s','struct','structure'}
        if isdataset('dataset')
            [m,n] = size(dataset);
            s = struct;
            for j = 1:n
                s.(cols{j}) = ds.(cols{j});
            end
            T = s;
        end
    case {'v','vars','vects','vectors'}
        %<TODO>
    case 'string'
        %<TODO: Convert to tab-separated string>
        %...
    case {[],''} 
        % do nothing
end

%% Convert Columns Containing Text Data)
for j = 1:n
    if istext(T.(cols{j}))
        T.(cols{j}) = converttext(T.(cols{j}),texttype);
    end
end
