function b = isdataset(X)
% ISDATASET  True for dataset array.

b = isa(X,'dataset');