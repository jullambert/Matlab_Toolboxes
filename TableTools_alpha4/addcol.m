function ds = addcol(ds,colname,coltype)
% ADDCOL  Add empty column to dataset.
%    DS = ADDCOL(DS,colname,coltype)
%
%    <todo: ordinal levels>

nrows = size(ds,1);

switch coltype
    case 'nominal'
        warning off stats:dataset:subsasgn:DefaultValuesAdded
        ds.(colname) = nominal;
    case 'ordinal'
        warning off stats:dataset:subsasgn:DefaultValuesAdded
        ds.(colname) = ordinal;
    case 'double'
        ds.(colname) = zeros(nrows,1) + NaN;
    case {'single','int8','int16','int32','int64','uint8','uint16','uint32','uint64'}
        ds.(colname) = zeros(nrows,1,coltype) + NaN;
    case 'logical'
        ds.(colname) = false(nrows,1);
    case 'char'
        ds.(colname) = char(zeros(nrows,1));
    case 'cell'
        ds.(colname) = cell(nrows,1);
    otherwise
        error('Unknown type.')
end